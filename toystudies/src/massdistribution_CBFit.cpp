#include "string"
#include "vector"
#include "iostream"
#include "RooBMixDecay.h"
#include "RooCategory.h"
#include "RooDataSet.h"
#include "RooFitResult.h"
#include "RooGenericPdf.h"
#include "RooPlot.h"
#include "RooRealVar.h"
#include "RooSimPdfBuilder.h"
#include "RooSimultaneous.h"
#include "RooWorkspace.h"
#include "RooSimWSTool.h"

#include "doofit/builder/EasyPdf/EasyPdf.h"
#include "doofit/config/CommonConfig.h"
#include "doofit/fitter/easyfit/FitResultPrinter.h"
#include "doofit/fitter/easyfit/EasyFit.h"
#include "doofit/toy/ToyFactoryStd/ToyFactoryStd.h"
#include "doofit/toy/ToyFactoryStd/ToyFactoryStdConfig.h"
#include "doofit/toy/ToyStudyStd/ToyStudyStd.h"
#include "doofit/toy/ToyStudyStd/ToyStudyStdConfig.h"

#include "doofit/plotting/Plot/Plot.h"
#include "doofit/plotting/Plot/PlotConfig.h"

#include "RooAbsPdf.h"
#include "RooExtendPdf.h"
#include "Urania/DecRateCoeff.h"
#include "doofit/roofit/pdfs/DooCubicSplinePdf.h"
#include "RooProdPdf.h"
#include "RooFitResult.h"
#include "TMatrixT.h"


//new (compared to massdistribution.cpp)
#include "RooCBShape.h"



using namespace RooFit;


int main(int argc, char* argv[]) {
    
    doofit::builder::EasyPdf *epdf = new doofit::builder::EasyPdf();
    
    //number of events
    epdf->Var("sig_yield");
    epdf->Var("sig_yield").setVal(5000);

    //mass
    epdf->Var("obsMass");
    epdf->Var("obsMass").SetTitle("m_{#kern[0.2]{DD}}");
    epdf->Var("obsMass").setUnit("MeV");
    epdf->Var("obsMass").setMin(5155);
    epdf->Var("obsMass").setMax(5326.79);

    
    //Zusammenfassen der Parameter in einem RooArgSet
    RooArgSet Observables;
    Observables.add(RooArgSet(epdf->Var("obsMass")));
    
    
    ///////////////////Generiere PDF's/////////////////////
    //Signal
    //aktuell nur einfacher Gauß und versuchen dies hier ans laufen zu bekommen!!!!!!!!!!!!!!!!!!!!
    epdf->GaussModel("pdf_sig_mass_gauss", epdf->Var("obsMass"),epdf->Var("signal_pdf_mean"), epdf-> Var("signal_pdf_sigma"));
    epdf->Extend("pdfExtend",epdf->Pdf("pdf_sig_mass_gauss"),epdf->Real("sig_yield"));


    RooWorkspace ws;
    ws.import(epdf->Pdf("pdfExtend"));
    ws.defineSet("Observables",Observables, true);
    ws.Print();
    
    doofit::config::CommonConfig cfg_com("common");
    cfg_com.InitializeOptions(argc, argv);
    doofit::toy::ToyFactoryStdConfig cfg_tfac("toyfac");
    cfg_tfac.InitializeOptions(cfg_com);
    doofit::toy::ToyStudyStdConfig cfg_tstudy("toystudy");
    cfg_tstudy.InitializeOptions(cfg_tfac);
    doofit::plotting::PlotConfig cfg_plot("cfg_plot");
    cfg_plot.InitializeOptions(cfg_plot);
                    
    cfg_tfac.set_workspace(&ws);
    cfg_tfac.set_generation_pdf_workspace("pdfExtend");
    cfg_com.CheckHelpFlagAndPrintHelp();
    cfg_com.PrintAll();
    
    doofit::toy::ToyFactoryStd tfac(cfg_com, cfg_tfac);
    doofit::toy::ToyStudyStd tstudy(cfg_com, cfg_tstudy, cfg_plot);
   

    /*
    ######################## Getrenntes FitModell ########################
    */ 

    epdf->CBShape("pdf_sig_mass_1stCB_fit", epdf->Var("obsMass"),epdf->Var("signal_pdf_1st_mean_fit"), epdf->Var("signal_pdf_1st_sigma_fit"),epdf->Var("signal_pdf_1st_alpha_fit"), epdf->Var("signal_pdf_1st_n_fit"));
    epdf->CBShape("pdf_sig_mass_2ndCB_fit", epdf->Var("obsMass"),epdf->Var("signal_pdf_2nd_mean_fit"), epdf->Var("signal_pdf_2nd_sigma_fit"),epdf->Var("signal_pdf_2nd_alpha_fit"), epdf->Var("signal_pdf_2nd_n_fit"));
    epdf->CBShape("pdf_sig_mass_3rdCB_fit", epdf->Var("obsMass"),epdf->Var("signal_pdf_3rd_mean_fit"), epdf->Var("signal_pdf_3rd_sigma_fit"),epdf->Var("signal_pdf_3rd_alpha_fit"), epdf->Var("signal_pdf_3rd_n_fit")); 

    epdf->Add("pdf_sig_mass_3CB_fit", RooArgList(epdf->Pdf("pdf_sig_mass_1stCB_fit"), epdf->Pdf("pdf_sig_mass_2ndCB_fit"), epdf->Pdf("pdf_sig_mass_3rdCB_fit")), RooArgList(epdf->Var("pdf_sig_mass_frac1_fit"),epdf->Var("pdf_sig_mass_frac2_fit")));
    epdf->Extend("pdfExtendFit", epdf->Pdf("pdf_sig_mass_3CB_fit"), epdf->Real("sig_yield"));




    //epdf->GaussModel("pdf_sig_mass_gauss_fit", epdf->Var("obsMass"),epdf->Var("signal_pdf_mean_fit"), epdf-> Var("signal_pdf_sigma_fit"));
    //epdf->Extend("pdfExtendFit", epdf->Pdf("pdf_sig_mass_gauss_fit"),epdf->Real("sig_yield"));




    for(int i=0;i<50;i++)
    { 
   
    RooDataSet* data = tfac.Generate();
    data->Print(); 
    //epdf->Pdf("pdfExtend").getParameters(data)->readFromFile("/home/delten/master-repositories/Toystudies_Master/parameters/massdistribution_CBFit-parameter.txt");
    //epdf->Pdf("pdfExtend").getParameters(data)->writeToFile("/home/delten/master-repositories/Toystudies_Master/parameters/massdistribution_CBFit-parameter.txt.new");
    epdf->Pdf("pdfExtend").getParameters(data)->readFromFile("/home/delten/repos/Toystudies_Master/parameters/massdistribution_CBFit-parameter.txt");
    epdf->Pdf("pdfExtend").getParameters(data)->writeToFile("/home/delten/repos/Toystudies_Master/parameters/massdistribution_CBFit-parameter.txt.new");    
    
    /*
     ##########################################   Zum getrennten generieren und fitten #####################
     */
    
    //
    //epdf->GaussModel("pdf_sig_mass_gauss_fit", epdf->Var("obsMass"),epdf->Var("signal_pdf_mean_fit"), epdf-> Var("signal_pdf_sigma_fit"));
    //epdf->Extend("pdfExtendFit", epdf->Pdf("pdf_sig_mass_gauss_fit"),epdf->Real("sig_yield"));
    //epdf->Pdf("pdfExtendFit").getParameters(data)->readFromFile("/home/delten/master-repositories/Toystudies_Master/parameters/massdistribution_CBFit-parameter.txt");
    //epdf->Pdf("pdfExtendFit").getParameters(data)->writeToFile("/home/delten/master-repositories/Toystudies_Master/parameters/massdistribution_CBFit-parameter.txt.new");
    epdf->Pdf("pdfExtendFit").getParameters(data)->readFromFile("/home/delten/repos/Toystudies_Master/parameters/massdistribution_CBFit-parameter.txt");
    epdf->Pdf("pdfExtendFit").getParameters(data)->writeToFile("/home/delten/repos/Toystudies_Master/parameters/massdistribution_CBFit-parameter.txt.new"); 
    
    RooFitResult* fit_result = epdf->Pdf("pdfExtendFit").fitTo(*data, NumCPU(4), Save(true), Minos(true), Verbose(false), Timer(true), Extended(true),Optimize(0));//,RooFit::Save(true));
    //tstudy.StoreFitResult(fit_result);
    epdf->Pdf("pdfExtendFit").getParameters(data)->writeToFile("/home/delten/repos/Toystudies_Master/fitresults/fitresults-massdistribution_CBFit.txt");
    
    /*
     #############################################################################################################################################
     */
    
                    
    //print results on terminal
    fit_result->Print();
    TMatrixTSym<double> corMatrix = fit_result->correlationMatrix();
    corMatrix.Print();
    
    //RooArgSets fürs Plotten
    RooArgSet plotobsMass;
    plotobsMass.add(RooArgSet(epdf->Var("obsMass")));
    

    using namespace doofit::plotting;

                    
    cfg_plot.set_plot_directory("/fhgfs/users/delten/DooSoftwareToyStudies/plots/massdistribution-CBFit");
    // plot PDF and directly specify components

    Plot myplot(cfg_plot, epdf->Var("obsMass"), *data, RooArgList(epdf->Pdf("pdfExtendFit")));
    myplot.AddPlotArg(ProjWData(plotobsMass, *data, true));
    myplot.PlotIt();      //plotting command
    
    //Zeilen zur Auswertung mit phido (jobs) zur Erstellung einer .root-Datei, auch Überprüfunh lhcb ob .root-Datei erstellt wurde
    tstudy.StoreFitResult(fit_result);
    }

    return 0;
    
}
