#include "string"
#include "vector"
#include "iostream"
#include "RooBMixDecay.h"
#include "RooCategory.h"
#include "RooDataSet.h"
#include "RooFitResult.h"
#include "RooGenericPdf.h"
#include "RooPlot.h"
#include "RooRealVar.h"
#include "RooSimPdfBuilder.h"
#include "RooSimultaneous.h"
#include "RooWorkspace.h"
#include "RooSimWSTool.h"

#include "doofit/builder/EasyPdf/EasyPdf.h"
#include "doofit/config/CommonConfig.h"
#include "doofit/fitter/easyfit/FitResultPrinter.h"
#include "doofit/fitter/easyfit/EasyFit.h"
#include "doofit/toy/ToyFactoryStd/ToyFactoryStd.h"
#include "doofit/toy/ToyFactoryStd/ToyFactoryStdConfig.h"
#include "doofit/toy/ToyStudyStd/ToyStudyStd.h"
#include "doofit/toy/ToyStudyStd/ToyStudyStdConfig.h"

#include "doofit/plotting/Plot/Plot.h"
#include "doofit/plotting/Plot/PlotConfig.h"

#include "RooAbsPdf.h"
#include "RooExtendPdf.h"
#include "Urania/DecRateCoeff.h"
#include "doofit/roofit/pdfs/DooCubicSplinePdf.h"
#include "RooProdPdf.h"
#include "RooFitResult.h"
#include "TMatrixT.h"


//new (compared to massdistribution.cpp)
#include "RooCBShape.h"

//new (compared to massdistribution_CBFit.cpp)
#include "RooExponential.h"

#include "RooSimultaneous.h"
#include "RooCategory.h"
#include "RooSuperCategory.h"
#include "RooGaussian.h"


using namespace RooFit;


int main(int argc, char* argv[]) {
    
    doofit::builder::EasyPdf *epdf = new doofit::builder::EasyPdf();
    
    //mass
    epdf->Var("obsMass");
    epdf->Var("obsMass").SetTitle("m_{#kern[0.2]{DD}}");
    epdf->Var("obsMass").setUnit("MeV");
    //Grenzen noch anpassen, abhängig vom Notebook
    epdf->Var("obsMass").setMin(5200);
    epdf->Var("obsMass").setMax(5537.28);

    
    //Zusammenfassen der Parameter in einem RooArgSet
    RooArgSet Observables;
    Observables.add(RooArgSet(epdf->Var("obsMass")));
    
    
    ///////////////////Generiere PDF's/////////////////////
    
    //Bd-Signal
    //Kpipi / Kpipi
    epdf->CBShape("Bd_pdf_sig_mass_1stCB_Kpipi", epdf->Var("obsMass"),epdf->Var("Bd_signal_pdf_mean_Kpipi"), epdf->Var("Bd_signal_pdf_1st_sigma_mc_Kpipi"),epdf->Var("Bd_signal_pdf_1st_alpha_Kpipi"), epdf->Var("Bd_signal_pdf_n_Kpipi"));
    epdf->CBShape("Bd_pdf_sig_mass_2ndCB_Kpipi", epdf->Var("obsMass"),epdf->Var("Bd_signal_pdf_mean_Kpipi"), epdf->Var("Bd_signal_pdf_2nd_sigma_mc_Kpipi"),epdf->Var("Bd_signal_pdf_2nd_alpha_Kpipi"), epdf->Var("Bd_signal_pdf_n_Kpipi"));
    epdf->CBShape("Bd_pdf_sig_mass_3rdCB_Kpipi", epdf->Var("obsMass"),epdf->Var("Bd_signal_pdf_mean_Kpipi"), epdf->Var("Bd_signal_pdf_3rd_sigma_mc_Kpipi"),epdf->Var("Bd_signal_pdf_3rd_alpha_Kpipi"), epdf->Var("Bd_signal_pdf_n_Kpipi"));
    
    epdf->Add("Bd_pdf_sig_mass_3CB_Kpipi", RooArgList(epdf->Pdf("Bd_pdf_sig_mass_1stCB_Kpipi"), epdf->Pdf("Bd_pdf_sig_mass_2ndCB_Kpipi"), epdf->Pdf("Bd_pdf_sig_mass_3rdCB_Kpipi")), RooArgList(epdf->Var("Bd_pdf_sig_mass_frac1_Kpipi"),epdf->Var("Bd_pdf_sig_mass_frac2_Kpipi")));
    
    //KKpi / Kpipi
    epdf->CBShape("Bd_pdf_sig_mass_1stCB_KKpi", epdf->Var("obsMass"),epdf->Var("Bd_signal_pdf_mean_KKpi"), epdf->Var("Bd_signal_pdf_1st_sigma_mc_KKpi"),epdf->Var("Bd_signal_pdf_1st_alpha_KKpi"), epdf->Var("Bd_signal_pdf_n_KKpi"));
    epdf->CBShape("Bd_pdf_sig_mass_2ndCB_KKpi", epdf->Var("obsMass"),epdf->Var("Bd_signal_pdf_mean_KKpi"), epdf->Var("Bd_signal_pdf_2nd_sigma_mc_KKpi"),epdf->Var("Bd_signal_pdf_2nd_alpha_KKpi"), epdf->Var("Bd_signal_pdf_n_KKpi"));
    epdf->CBShape("Bd_pdf_sig_mass_3rdCB_KKpi", epdf->Var("obsMass"),epdf->Var("Bd_signal_pdf_mean_KKpi"), epdf->Var("Bd_signal_pdf_3rd_sigma_mc_KKpi"),epdf->Var("Bd_signal_pdf_3rd_alpha_KKpi"), epdf->Var("Bd_signal_pdf_n_KKpi"));

    epdf->Add("Bd_pdf_sig_mass_3CB_KKpi", RooArgList(epdf->Pdf("Bd_pdf_sig_mass_1stCB_KKpi"), epdf->Pdf("Bd_pdf_sig_mass_2ndCB_KKpi"), epdf->Pdf("Bd_pdf_sig_mass_3rdCB_KKpi")), RooArgList(epdf->Var("Bd_pdf_sig_mass_frac1_KKpi"),epdf->Var("Bd_pdf_sig_mass_frac2_KKpi")));


    //Bs-Signal
    //Kpipi / Kpipi
    epdf->CBShape("Bs_pdf_sig_mass_1stCB_Kpipi", epdf->Var("obsMass"), epdf->Var("Bs_signal_pdf_mean_Kpipi"), epdf->Var("Bs_signal_pdf_1st_sigma_mc_Kpipi"), epdf->Var("Bs_signal_pdf_1st_alpha_Kpipi"), epdf->Var("Bs_signal_pdf_n_Kpipi"));
    epdf->CBShape("Bs_pdf_sig_mass_2ndCB_Kpipi", epdf->Var("obsMass"), epdf->Var("Bs_signal_pdf_mean_Kpipi"), epdf->Var("Bs_signal_pdf_2nd_sigma_mc_Kpipi"), epdf->Var("Bs_signal_pdf_2nd_alpha_Kpipi"), epdf->Var("Bs_signal_pdf_n_Kpipi"));
    epdf->CBShape("Bs_pdf_sig_mass_3rdCB_Kpipi", epdf->Var("obsMass"), epdf->Var("Bs_signal_pdf_mean_Kpipi"), epdf->Var("Bs_signal_pdf_3rd_sigma_mc_Kpipi"), epdf->Var("Bs_signal_pdf_3rd_alpha_Kpipi"), epdf->Var("Bs_signal_pdf_n_Kpipi"));    

    epdf->Add("Bs_pdf_sig_mass_3CB_Kpipi", RooArgList(epdf->Pdf("Bs_pdf_sig_mass_1stCB_Kpipi"), epdf->Pdf("Bs_pdf_sig_mass_2ndCB_Kpipi"), epdf->Pdf("Bs_pdf_sig_mass_3rdCB_Kpipi")), RooArgList(epdf->Var("Bs_pdf_sig_mass_frac1_Kpipi"),epdf->Var("Bs_pdf_sig_mass_frac2_Kpipi")));
    
    //KKpi / Kpipi
    epdf->CBShape("Bs_pdf_sig_mass_1stCB_KKpi", epdf->Var("obsMass"), epdf->Var("Bs_signal_pdf_mean_KKpi"), epdf->Var("Bs_signal_pdf_1st_sigma_mc_KKpi"), epdf->Var("Bs_signal_pdf_1st_alpha_KKpi"), epdf->Var("Bs_signal_pdf_n_KKpi"));
    epdf->CBShape("Bs_pdf_sig_mass_2ndCB_KKpi", epdf->Var("obsMass"), epdf->Var("Bs_signal_pdf_mean_KKpi"), epdf->Var("Bs_signal_pdf_2nd_sigma_mc_KKpi"), epdf->Var("Bs_signal_pdf_2nd_alpha_KKpi"), epdf->Var("Bs_signal_pdf_n_KKpi"));
    epdf->CBShape("Bs_pdf_sig_mass_3rdCB_KKpi", epdf->Var("obsMass"), epdf->Var("Bs_signal_pdf_mean_KKpi"), epdf->Var("Bs_signal_pdf_3rd_sigma_mc_KKpi"), epdf->Var("Bs_signal_pdf_3rd_alpha_KKpi"), epdf->Var("Bs_signal_pdf_n_KKpi"));

    epdf->Add("Bs_pdf_sig_mass_3CB_KKpi", RooArgList(epdf->Pdf("Bs_pdf_sig_mass_1stCB_KKpi"), epdf->Pdf("Bs_pdf_sig_mass_2ndCB_KKpi"), epdf->Pdf("Bs_pdf_sig_mass_3rdCB_KKpi")), RooArgList(epdf->Var("Bs_pdf_sig_mass_frac1_KKpi"),epdf->Var("Bs_pdf_sig_mass_frac2_KKpi")));


    //Background
    epdf->Exponential("pdf_bkg_mass_exp_Kpipi", epdf->Var("obsMass"), epdf->Var("background_pdf_lambda_Kpipi"));
    epdf->Exponential("pdf_bkg_mass_exp_KKpi", epdf->Var("obsMass"), epdf->Var("background_pdf_lambda_KKpi"));

 
    
    //Yields
    //epdf->Formula("Bs_sig_yield_Kpipi","@0*@1*@2*@3",RooArgList(epdf->Var("Bd_sig_yield_Kpipi"),epdf->Var("branching_factor"),epdf->Var("fragmentation_factor"),epdf->Var("scaling_factor_run2")));
    //epdf->Formula("Bs_sig_yield_KKpi","@0*@1*@2*@3",RooArgList(epdf->Var("Bd_sig_yield_KKpi"),epdf->Var("branching_factor"),epdf->Var("fragmentation_factor"),epdf->Var("scaling_factor_run2")));

    //epdf->Formula("Bs_sig_yield_Kpipi","@0*@1*@2*@3*@4",RooArgList(epdf->Var("Bd_sig_yield_Kpipi"),epdf->Var("branching_factor"),epdf->Var("fragmentation_factor"),epdf->Var("scaling_factor_run2"),epdf->Var("efficiencies")));
    //epdf->Formula("Bs_sig_yield_KKpi","@0*@1*@2*@3*@4",RooArgList(epdf->Var("Bd_sig_yield_KKpi"),epdf->Var("branching_factor"),epdf->Var("fragmentation_factor"),epdf->Var("scaling_factor_run2"),epdf->Var("efficiencies")));

    epdf->Formula("Bs_sig_yield_Kpipi","@0*@1*@2*@3*@4",RooArgList(epdf->Var("Bd_sig_yield_Kpipi"),epdf->Var("branching_factor"),epdf->Var("fragmentation_factor"),epdf->Var("scaling_factor_run2"),epdf->Var("efficiencies_Kpipi")));
    epdf->Formula("Bs_sig_yield_KKpi","@0*@1*@2*@3*@4",RooArgList(epdf->Var("Bd_sig_yield_KKpi"),epdf->Var("branching_factor"),epdf->Var("fragmentation_factor"),epdf->Var("scaling_factor_run2"),epdf->Var("efficiencies_KKpi")));
 
    //zusammenfuehren
    epdf->Add("pdfExtend_Kpipi",RooArgList(epdf->Pdf("Bd_pdf_sig_mass_3CB_Kpipi"),epdf->Pdf("Bs_pdf_sig_mass_3CB_Kpipi"),epdf->Pdf("pdf_bkg_mass_exp_Kpipi")),RooArgList(epdf->Var("Bd_sig_yield_Kpipi"),epdf->Formula("Bs_sig_yield_Kpipi"),epdf->Var("bkg_yield_Kpipi")));
    epdf->Add("pdfExtend_KKpi",RooArgList(epdf->Pdf("Bd_pdf_sig_mass_3CB_KKpi"),epdf->Pdf("Bs_pdf_sig_mass_3CB_KKpi"),epdf->Pdf("pdf_bkg_mass_exp_KKpi")),RooArgList(epdf->Var("Bd_sig_yield_KKpi"),epdf->Formula("Bs_sig_yield_KKpi"),epdf->Var("bkg_yield_KKpi")));


    RooCategory category("category","category");
    category.defineType("Kpipi");    
    category.defineType("KKpi"); 
    
    
    RooSimultaneous simPdf("simPdf","simPdf",category);
    simPdf.addPdf(epdf->Pdf("pdfExtend_Kpipi"),"Kpipi");
    simPdf.addPdf(epdf->Pdf("pdfExtend_KKpi"),"KKpi");


  

    simPdf.getParameters(new RooDataSet())->readFromFile("/home/delten/repos/Toystudies_Master/parameters/finalstudy_BdBs_Shared-parameter.txt");

 
    RooWorkspace ws;
    ws.import(simPdf);
    ws.defineSet("Observables",Observables, true);
    ws.Print();
    
    doofit::config::CommonConfig cfg_com("common");
    cfg_com.InitializeOptions(argc, argv);
    doofit::toy::ToyFactoryStdConfig cfg_tfac("toyfac");
    cfg_tfac.InitializeOptions(cfg_com);
    doofit::toy::ToyStudyStdConfig cfg_tstudy("toystudy");
    cfg_tstudy.InitializeOptions(cfg_tfac);
    doofit::plotting::PlotConfig cfg_plot("cfg_plot");
    cfg_plot.InitializeOptions(cfg_plot);
                    
    cfg_tfac.set_workspace(&ws);
    cfg_tfac.set_generation_pdf_workspace("simPdf");
    cfg_com.CheckHelpFlagAndPrintHelp();
    cfg_com.PrintAll();
    
    doofit::toy::ToyFactoryStd tfac(cfg_com, cfg_tfac);
    doofit::toy::ToyStudyStd tstudy(cfg_com, cfg_tstudy, cfg_plot);
   

    /*
    ######################## Getrenntes FitModell ########################
    */ 

   //sigma scaling factor
    epdf->Formula("Bd_signal_pdf_1st_sigma_Kpipi_fit","@0*@1",RooArgList(epdf->Var("Bd_signal_pdf_1st_sigma_mc_Kpipi"),epdf->Var("Bd_scaling_factor_Kpipi_fit")));
    epdf->Formula("Bd_signal_pdf_2nd_sigma_Kpipi_fit","@0*@1",RooArgList(epdf->Var("Bd_signal_pdf_2nd_sigma_mc_Kpipi"),epdf->Var("Bd_scaling_factor_Kpipi_fit")));

    epdf->Formula("Bd_signal_pdf_1st_sigma_KKpi_fit","@0*@1",RooArgList(epdf->Var("Bd_signal_pdf_1st_sigma_mc_KKpi"),epdf->Var("Bd_scaling_factor_KKpi_fit")));
    epdf->Formula("Bd_signal_pdf_2nd_sigma_KKpi_fit","@0*@1",RooArgList(epdf->Var("Bd_signal_pdf_2nd_sigma_mc_KKpi"),epdf->Var("Bd_scaling_factor_KKpi_fit")));



    //Bd-Signal
    //Kpipi / Kpipi
    //mean getrennt
    //epdf->CBShape("Bd_pdf_sig_mass_1stCB_Kpipi_fit", epdf->Var("obsMass"), epdf->Var("Bd_signal_pdf_mean_Kpipi_fit"), epdf->Formula("Bd_signal_pdf_1st_sigma_Kpipi_fit"), epdf->Var("Bd_signal_pdf_1st_alpha_Kpipi_fit"), epdf->Var("signal_pdf_n_Kpipi_fit"));
    //epdf->CBShape("Bd_pdf_sig_mass_2ndCB_Kpipi_fit", epdf->Var("obsMass"), epdf->Var("Bd_signal_pdf_mean_Kpipi_fit"), epdf->Formula("Bd_signal_pdf_2nd_sigma_Kpipi_fit"), epdf->Var("Bd_signal_pdf_2nd_alpha_Kpipi_fit"), epdf->Var("signal_pdf_n_Kpipi_fit"));
    //epdf->CBShape("Bd_pdf_sig_mass_3rdCB_Kpipi_fit", epdf->Var("obsMass"), epdf->Var("Bd_signal_pdf_mean_Kpipi_fit"), epdf->Var("Bd_signal_pdf_3rd_sigma_mc_Kpipi_fit"), epdf->Var("Bd_signal_pdf_3rd_alpha_Kpipi_fit"), epdf->Var("signal_pdf_n_Kpipi_fit"));

    epdf->CBShape("Bd_pdf_sig_mass_1stCB_Kpipi_fit", epdf->Var("obsMass"), epdf->Var("Bd_signal_pdf_mean_fit"), epdf->Formula("Bd_signal_pdf_1st_sigma_Kpipi_fit"), epdf->Var("Bd_signal_pdf_1st_alpha_Kpipi_fit"), epdf->Var("signal_pdf_n_Kpipi_fit"));
    epdf->CBShape("Bd_pdf_sig_mass_2ndCB_Kpipi_fit", epdf->Var("obsMass"), epdf->Var("Bd_signal_pdf_mean_fit"), epdf->Formula("Bd_signal_pdf_2nd_sigma_Kpipi_fit"), epdf->Var("Bd_signal_pdf_2nd_alpha_Kpipi_fit"), epdf->Var("signal_pdf_n_Kpipi_fit"));
    epdf->CBShape("Bd_pdf_sig_mass_3rdCB_Kpipi_fit", epdf->Var("obsMass"), epdf->Var("Bd_signal_pdf_mean_fit"), epdf->Var("Bd_signal_pdf_3rd_sigma_mc_Kpipi_fit"), epdf->Var("Bd_signal_pdf_3rd_alpha_Kpipi_fit"), epdf->Var("signal_pdf_n_Kpipi_fit"));



    epdf->Add("Bd_pdf_sig_mass_3CB_Kpipi_fit", RooArgList(epdf->Pdf("Bd_pdf_sig_mass_1stCB_Kpipi_fit"), epdf->Pdf("Bd_pdf_sig_mass_2ndCB_Kpipi_fit"), epdf->Pdf("Bd_pdf_sig_mass_3rdCB_Kpipi_fit")), RooArgList(epdf->Var("Bd_pdf_sig_mass_frac1_Kpipi_fit"),epdf->Var("Bd_pdf_sig_mass_frac2_Kpipi_fit")));


    //KKpi / Kpipi
    //mean getrennt
    //epdf->CBShape("Bd_pdf_sig_mass_1stCB_KKpi_fit", epdf->Var("obsMass"), epdf->Var("Bd_signal_pdf_mean_KKpi_fit"), epdf->Formula("Bd_signal_pdf_1st_sigma_KKpi_fit"), epdf->Var("Bd_signal_pdf_1st_alpha_KKpi_fit"), epdf->Var("signal_pdf_n_KKpi_fit"));
    //epdf->CBShape("Bd_pdf_sig_mass_2ndCB_KKpi_fit", epdf->Var("obsMass"), epdf->Var("Bd_signal_pdf_mean_KKpi_fit"), epdf->Formula("Bd_signal_pdf_2nd_sigma_KKpi_fit"), epdf->Var("Bd_signal_pdf_2nd_alpha_KKpi_fit"), epdf->Var("signal_pdf_n_KKpi_fit"));
    //epdf->CBShape("Bd_pdf_sig_mass_3rdCB_KKpi_fit", epdf->Var("obsMass"), epdf->Var("Bd_signal_pdf_mean_KKpi_fit"), epdf->Var("Bd_signal_pdf_3rd_sigma_mc_KKpi_fit"), epdf->Var("Bd_signal_pdf_3rd_alpha_KKpi_fit"), epdf->Var("signal_pdf_n_KKpi_fit"));

    epdf->CBShape("Bd_pdf_sig_mass_1stCB_KKpi_fit", epdf->Var("obsMass"), epdf->Var("Bd_signal_pdf_mean_fit"), epdf->Formula("Bd_signal_pdf_1st_sigma_KKpi_fit"), epdf->Var("Bd_signal_pdf_1st_alpha_KKpi_fit"), epdf->Var("signal_pdf_n_KKpi_fit"));
    epdf->CBShape("Bd_pdf_sig_mass_2ndCB_KKpi_fit", epdf->Var("obsMass"), epdf->Var("Bd_signal_pdf_mean_fit"), epdf->Formula("Bd_signal_pdf_2nd_sigma_KKpi_fit"), epdf->Var("Bd_signal_pdf_2nd_alpha_KKpi_fit"), epdf->Var("signal_pdf_n_KKpi_fit"));
    epdf->CBShape("Bd_pdf_sig_mass_3rdCB_KKpi_fit", epdf->Var("obsMass"), epdf->Var("Bd_signal_pdf_mean_fit"), epdf->Var("Bd_signal_pdf_3rd_sigma_mc_KKpi_fit"), epdf->Var("Bd_signal_pdf_3rd_alpha_KKpi_fit"), epdf->Var("signal_pdf_n_KKpi_fit"));

    epdf->Add("Bd_pdf_sig_mass_3CB_KKpi_fit", RooArgList(epdf->Pdf("Bd_pdf_sig_mass_1stCB_KKpi_fit"), epdf->Pdf("Bd_pdf_sig_mass_2ndCB_KKpi_fit"), epdf->Pdf("Bd_pdf_sig_mass_3rdCB_KKpi_fit")), RooArgList(epdf->Var("Bd_pdf_sig_mass_frac1_KKpi_fit"),epdf->Var("Bd_pdf_sig_mass_frac2_KKpi_fit")));



    //BsMean
    //mean_difference getrennt
    //epdf->Formula("Bs_signal_pdf_mean_Kpipi_fit","@0+@1",RooArgList(epdf->Var("Bd_signal_pdf_mean_fit_Kpipi"),epdf->Var("mean_difference_Kpipi")));
    //epdf->Formula("Bs_signal_pdf_mean_KKpi_fit","@0+@1",RooArgList(epdf->Var("Bd_signal_pdf_mean_fit_KKpi"),epdf->Var("mean_difference_KKpi")));

    epdf->Formula("Bs_signal_pdf_mean_Kpipi_fit","@0+@1",RooArgList(epdf->Var("Bd_signal_pdf_mean_fit"),epdf->Var("mean_difference")));
    epdf->Formula("Bs_signal_pdf_mean_KKpi_fit","@0+@1",RooArgList(epdf->Var("Bd_signal_pdf_mean_fit"),epdf->Var("mean_difference")));


    //Bs-Signal
    //Kpipi & Kpipi
    epdf->CBShape("Bs_pdf_sig_mass_1stCB_Kpipi_fit", epdf->Var("obsMass"),epdf->Formula("Bs_signal_pdf_mean_Kpipi_fit"), epdf->Formula("Bd_signal_pdf_1st_sigma_Kpipi_fit"),epdf->Var("Bd_signal_pdf_1st_alpha_Kpipi_fit"), epdf->Var("signal_pdf_n_Kpipi_fit"));
    epdf->CBShape("Bs_pdf_sig_mass_2ndCB_Kpipi_fit", epdf->Var("obsMass"),epdf->Formula("Bs_signal_pdf_mean_Kpipi_fit"), epdf->Formula("Bd_signal_pdf_2nd_sigma_Kpipi_fit"),epdf->Var("Bd_signal_pdf_2nd_alpha_Kpipi_fit"), epdf->Var("signal_pdf_n_Kpipi_fit"));
    epdf->CBShape("Bs_pdf_sig_mass_3rdCB_Kpipi_fit", epdf->Var("obsMass"),epdf->Formula("Bs_signal_pdf_mean_Kpipi_fit"), epdf->Var("Bd_signal_pdf_3rd_sigma_mc_Kpipi_fit"),epdf->Var("Bd_signal_pdf_3rd_alpha_Kpipi_fit"), epdf->Var("signal_pdf_n_Kpipi_fit"));
  
    epdf->Add("Bs_pdf_sig_mass_3CB_Kpipi_fit", RooArgList(epdf->Pdf("Bs_pdf_sig_mass_1stCB_Kpipi_fit"), epdf->Pdf("Bs_pdf_sig_mass_2ndCB_Kpipi_fit"), epdf->Pdf("Bs_pdf_sig_mass_3rdCB_Kpipi_fit")), RooArgList(epdf->Var("Bd_pdf_sig_mass_frac1_Kpipi_fit"),epdf->Var("Bd_pdf_sig_mass_frac2_Kpipi_fit")));



    //KKpi / Kpipi
    epdf->CBShape("Bs_pdf_sig_mass_1stCB_KKpi_fit", epdf->Var("obsMass"),epdf->Formula("Bs_signal_pdf_mean_KKpi_fit"), epdf->Formula("Bd_signal_pdf_1st_sigma_KKpi_fit"),epdf->Var("Bd_signal_pdf_1st_alpha_KKpi_fit"), epdf->Var("signal_pdf_n_KKpi_fit"));
    epdf->CBShape("Bs_pdf_sig_mass_2ndCB_KKpi_fit", epdf->Var("obsMass"),epdf->Formula("Bs_signal_pdf_mean_KKpi_fit"), epdf->Formula("Bd_signal_pdf_2nd_sigma_KKpi_fit"),epdf->Var("Bd_signal_pdf_2nd_alpha_KKpi_fit"), epdf->Var("signal_pdf_n_KKpi_fit"));
    epdf->CBShape("Bs_pdf_sig_mass_3rdCB_KKpi_fit", epdf->Var("obsMass"),epdf->Formula("Bs_signal_pdf_mean_KKpi_fit"), epdf->Var("Bd_signal_pdf_3rd_sigma_mc_KKpi_fit"),epdf->Var("Bd_signal_pdf_3rd_alpha_KKpi_fit"), epdf->Var("signal_pdf_n_KKpi_fit"));

    epdf->Add("Bs_pdf_sig_mass_3CB_KKpi_fit", RooArgList(epdf->Pdf("Bs_pdf_sig_mass_1stCB_Kpipi_fit"), epdf->Pdf("Bs_pdf_sig_mass_2ndCB_KKpi_fit"), epdf->Pdf("Bs_pdf_sig_mass_3rdCB_KKpi_fit")), RooArgList(epdf->Var("Bd_pdf_sig_mass_frac1_KKpi_fit"),epdf->Var("Bd_pdf_sig_mass_frac2_KKpi_fit")));



    //Background
    epdf->Exponential("pdf_bkg_mass_exp_Kpipi_fit", epdf->Var("obsMass"), epdf->Var("background_pdf_lambda_Kpipi_fit"));
    epdf->Exponential("pdf_bkg_mass_exp_KKpi_fit", epdf->Var("obsMass"), epdf->Var("background_pdf_lambda_KKpi_fit"));




    //zusammenfuehren
    epdf->Add("pdfExtendFit_Kpipi",RooArgList(epdf->Pdf("Bd_pdf_sig_mass_3CB_Kpipi_fit"),epdf->Pdf("Bs_pdf_sig_mass_3CB_Kpipi_fit"),epdf->Pdf("pdf_bkg_mass_exp_Kpipi_fit")),RooArgList(epdf->Var("Bd_sig_yield_Kpipi"),epdf->Formula("Bs_sig_yield_Kpipi"),epdf->Var("bkg_yield_Kpipi")));
    epdf->Add("pdfExtendFit_KKpi",RooArgList(epdf->Pdf("Bd_pdf_sig_mass_3CB_KKpi_fit"),epdf->Pdf("Bs_pdf_sig_mass_3CB_KKpi_fit"),epdf->Pdf("pdf_bkg_mass_exp_KKpi_fit")),RooArgList(epdf->Var("Bd_sig_yield_KKpi"),epdf->Formula("Bs_sig_yield_KKpi"),epdf->Var("bkg_yield_KKpi")));    
    

    RooSimultaneous simPdfFit("simPdfFit","simPdfFit",category);
    simPdfFit.addPdf(epdf->Pdf("pdfExtendFit_Kpipi"),"Kpipi");
    simPdfFit.addPdf(epdf->Pdf("pdfExtendFit_KKpi"),"KKpi");


    for(int i=0;i<200;i++)
    { 
   
    RooDataSet* data = tfac.Generate();
    data->Print(); 
    simPdf.getParameters(data)->writeToFile(("/home/delten/repos/Toystudies_Master/parameters/finalstudy_BdBs_Shared-parameter/finalstudy_BdBs_Shared-parameter.txt.new"+std::to_string(i)).c_str());

    /*
     ##########################################   Zum getrennten generieren und fitten #####################
     */
   

    simPdfFit.getParameters(data)->readFromFile("/home/delten/repos/Toystudies_Master/parameters/finalstudy_BdBs_Shared-parameter.txt");
    simPdfFit.getParameters(data)->writeToFile(("/home/delten/repos/Toystudies_Master/parameters/finalstudy_BdBs_Shared_fit-parameter/finalstudy_BdBs_Shared_fit-parameter.txt.new"+std::to_string(i)).c_str());
 
    
    RooFitResult* fit_result = simPdfFit.fitTo(*data, NumCPU(4), Save(true), Minos(true), Verbose(false), Timer(true), Extended(true),Optimize(0));//,RooFit::Save(true));
    //tstudy.StoreFitResult(fit_result);
    simPdfFit.getParameters(data)->writeToFile(("/home/delten/repos/Toystudies_Master/fitresults/fitresults-finalstudy_BdBs_Shared_fitresult/fitresults-finalstudy_BdBs_Shared_fitresult.txt"+std::to_string(i)).c_str());    
    /*
     #############################################################################################################################################
     */
    
                    
    //print results on terminal
    fit_result->Print();
    TMatrixTSym<double> corMatrix = fit_result->correlationMatrix();
    corMatrix.Print();
    
    //RooArgSets fürs Plotten
    RooArgSet plotobsMass;
    plotobsMass.add(RooArgSet(epdf->Var("obsMass")));
    

    using namespace doofit::plotting;

                    
    cfg_plot.set_plot_directory("/fhgfs/users/delten/DooSoftwareToyStudies/plots/finalstudy_BdBs_Shared");
    //path for phido when fhgfs is not available
    //cfg_plot.set_plot_directory("/home/delten/toystudies_plot_directory_fhgfs_down_test/massdistribution_3CBGenFitBdsharedBs_Yields_Bkg");
    // plot PDF and directly specify components

    Plot myplot(cfg_plot, epdf->Var("obsMass"), *data, RooArgList(simPdfFit));
    myplot.AddPlotArg(ProjWData(plotobsMass, *data, true));
    myplot.PlotIt();      //plotting command
    
    

    //Zeilen zur Auswertung mit phido (jobs) zur Erstellung einer .root-Datei, auch lokale Überprüfung ob .root-Datei erstellt wurde
    tstudy.StoreFitResult(fit_result);
    }

    tstudy.FinishFitResultSaving();  //works on phido
    tstudy.ReadFitResults();
    tstudy.EvaluateFitResults();
    tstudy.PlotEvaluatedParameters();


    return 0;
    
}
