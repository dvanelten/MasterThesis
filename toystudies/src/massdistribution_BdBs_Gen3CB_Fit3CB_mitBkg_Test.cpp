#include "string"
#include "vector"
#include "iostream"
#include "RooBMixDecay.h"
#include "RooCategory.h"
#include "RooDataSet.h"
#include "RooFitResult.h"
#include "RooGenericPdf.h"
#include "RooPlot.h"
#include "RooRealVar.h"
#include "RooSimPdfBuilder.h"
#include "RooSimultaneous.h"
#include "RooWorkspace.h"
#include "RooSimWSTool.h"

#include "doofit/builder/EasyPdf/EasyPdf.h"
#include "doofit/config/CommonConfig.h"
#include "doofit/fitter/easyfit/FitResultPrinter.h"
#include "doofit/fitter/easyfit/EasyFit.h"
#include "doofit/toy/ToyFactoryStd/ToyFactoryStd.h"
#include "doofit/toy/ToyFactoryStd/ToyFactoryStdConfig.h"
#include "doofit/toy/ToyStudyStd/ToyStudyStd.h"
#include "doofit/toy/ToyStudyStd/ToyStudyStdConfig.h"

#include "doofit/plotting/Plot/Plot.h"
#include "doofit/plotting/Plot/PlotConfig.h"

#include "RooAbsPdf.h"
#include "RooExtendPdf.h"
#include "Urania/DecRateCoeff.h"
#include "doofit/roofit/pdfs/DooCubicSplinePdf.h"
#include "RooProdPdf.h"
#include "RooFitResult.h"
#include "TMatrixT.h"


//new (compared to massdistribution.cpp)
#include "RooCBShape.h"

//new (compared to massdistribution_CBFit.cpp)
#include "RooExponential.h"


using namespace RooFit;


int main(int argc, char* argv[]) {
    
    doofit::builder::EasyPdf *epdf = new doofit::builder::EasyPdf();
    
    //mass
    epdf->Var("obsMass");
    epdf->Var("obsMass").SetTitle("m_{#kern[0.2]{DD}}");
    epdf->Var("obsMass").setUnit("MeV");
    //Grenzen noch anpassen, abhängig vom Notebook
    epdf->Var("obsMass").setMin(5200);
    epdf->Var("obsMass").setMax(5500);

    
    //Zusammenfassen der Parameter in einem RooArgSet
    RooArgSet Observables;
    Observables.add(RooArgSet(epdf->Var("obsMass")));
    
    
    ///////////////////Generiere PDF's/////////////////////
    
    //Bd-Signal
    epdf->CBShape("Bd_pdf_sig_mass_1stCB", epdf->Var("obsMass"),epdf->Var("Bd_signal_pdf_mean"), epdf->Var("Bd_signal_pdf_1st_sigma_mc"),epdf->Var("Bd_signal_pdf_1st_alpha"), epdf->Var("Bd_signal_pdf_1st_n"));
    epdf->CBShape("Bd_pdf_sig_mass_2ndCB", epdf->Var("obsMass"),epdf->Var("Bd_signal_pdf_mean"), epdf->Var("Bd_signal_pdf_2nd_sigma_mc"),epdf->Var("Bd_signal_pdf_2nd_alpha"), epdf->Var("Bd_signal_pdf_2nd_n"));
    epdf->CBShape("Bd_pdf_sig_mass_3rdCB", epdf->Var("obsMass"),epdf->Var("Bd_signal_pdf_mean"), epdf->Var("Bd_signal_pdf_3rd_sigma_mc"),epdf->Var("Bd_signal_pdf_3rd_alpha"), epdf->Var("Bd_signal_pdf_3rd_n"));
    
    epdf->Add("Bd_pdf_sig_mass_3CB", RooArgList(epdf->Pdf("Bd_pdf_sig_mass_1stCB"), epdf->Pdf("Bd_pdf_sig_mass_2ndCB"), epdf->Pdf("Bd_pdf_sig_mass_3rdCB")), RooArgList(epdf->Var("Bd_pdf_sig_mass_frac1"),epdf->Var("Bd_pdf_sig_mass_frac2")));
    
    //Bs-Signal
    epdf->CBShape("Bs_pdf_sig_mass_1stCB", epdf->Var("obsMass"), epdf->Var("Bs_signal_pdf_mean"), epdf->Var("Bs_signal_pdf_1st_sigma_mc"), epdf->Var("Bs_signal_pdf_1st_alpha"), epdf->Var("Bs_signal_pdf_1st_n"));
    epdf->CBShape("Bs_pdf_sig_mass_2ndCB", epdf->Var("obsMass"), epdf->Var("Bs_signal_pdf_mean"), epdf->Var("Bs_signal_pdf_2nd_sigma_mc"), epdf->Var("Bs_signal_pdf_2nd_alpha"), epdf->Var("Bs_signal_pdf_2nd_n"));
    epdf->CBShape("Bs_pdf_sig_mass_3rdCB", epdf->Var("obsMass"), epdf->Var("Bs_signal_pdf_mean"), epdf->Var("Bs_signal_pdf_3rd_sigma_mc"), epdf->Var("Bs_signal_pdf_3rd_alpha"), epdf->Var("Bs_signal_pdf_3rd_n"));    

    epdf->Add("Bs_pdf_sig_mass_3CB", RooArgList(epdf->Pdf("Bs_pdf_sig_mass_1stCB"), epdf->Pdf("Bs_pdf_sig_mass_2ndCB"), epdf->Pdf("Bs_pdf_sig_mass_3rdCB")), RooArgList(epdf->Var("Bs_pdf_sig_mass_frac1"),epdf->Var("Bs_pdf_sig_mass_frac2")));
    
    //Background
    epdf->Exponential("pdf_bkg_mass_exp", epdf->Var("obsMass"), epdf->Var("background_pdf_lambda"));
  
    //zusammenfuehren
    epdf->Add("pdfExtend",RooArgList(epdf->Pdf("Bd_pdf_sig_mass_3CB"),epdf->Pdf("Bs_pdf_sig_mass_3CB"),epdf->Pdf("pdf_bkg_mass_exp")),RooArgList(epdf->Var("Bd_sig_yield"),epdf->Var("Bs_sig_yield"),epdf->Var("bkg_yield")));
   
    RooWorkspace ws;
    ws.import(epdf->Pdf("pdfExtend"));
    ws.defineSet("Observables",Observables, true);
    ws.Print();
    
    doofit::config::CommonConfig cfg_com("common");
    cfg_com.InitializeOptions(argc, argv);
    doofit::toy::ToyFactoryStdConfig cfg_tfac("toyfac");
    cfg_tfac.InitializeOptions(cfg_com);
    doofit::toy::ToyStudyStdConfig cfg_tstudy("toystudy");
    cfg_tstudy.InitializeOptions(cfg_tfac);
    doofit::plotting::PlotConfig cfg_plot("cfg_plot");
    cfg_plot.InitializeOptions(cfg_plot);
                    
    cfg_tfac.set_workspace(&ws);
    cfg_tfac.set_generation_pdf_workspace("pdfExtend");
    cfg_com.CheckHelpFlagAndPrintHelp();
    cfg_com.PrintAll();
    
    doofit::toy::ToyFactoryStd tfac(cfg_com, cfg_tfac);
    doofit::toy::ToyStudyStd tstudy(cfg_com, cfg_tstudy, cfg_plot);
   

    /*
    ######################## Getrenntes FitModell ########################
    */ 

    epdf->Formula("Bd_signal_pdf_1st_sigma_fit","@0*@1",RooArgList(epdf->Var("Bd_signal_pdf_1st_sigma_mc"),epdf->Var("Bd_scaling_factor_fit")));
    epdf->Formula("Bd_signal_pdf_2nd_sigma_fit","@0*@1",RooArgList(epdf->Var("Bd_signal_pdf_2nd_sigma_mc"),epdf->Var("Bd_scaling_factor_fit")));

    //Bd-Signal
    epdf->CBShape("Bd_pdf_sig_mass_1stCB_fit", epdf->Var("obsMass"),epdf->Var("Bd_signal_pdf_mean_fit"), epdf->Formula("Bd_signal_pdf_1st_sigma_fit"),epdf->Var("Bd_signal_pdf_1st_alpha_fit"), epdf->Var("signal_pdf_n_fit"));
    epdf->CBShape("Bd_pdf_sig_mass_2ndCB_fit", epdf->Var("obsMass"),epdf->Var("Bd_signal_pdf_mean_fit"), epdf->Formula("Bd_signal_pdf_2nd_sigma_fit"),epdf->Var("Bd_signal_pdf_2nd_alpha_fit"), epdf->Var("signal_pdf_n_fit"));
    epdf->CBShape("Bd_pdf_sig_mass_3rdCB_fit", epdf->Var("obsMass"),epdf->Var("Bd_signal_pdf_mean_fit"), epdf->Var("Bd_signal_pdf_3rd_sigma_mc_fit"),epdf->Var("Bd_signal_pdf_3rd_alpha_fit"), epdf->Var("signal_pdf_n_fit"));

    epdf->Add("Bd_pdf_sig_mass_3CB_fit", RooArgList(epdf->Pdf("Bd_pdf_sig_mass_1stCB_fit"), epdf->Pdf("Bd_pdf_sig_mass_2ndCB_fit"), epdf->Pdf("Bd_pdf_sig_mass_3rdCB_fit")), RooArgList(epdf->Var("Bd_pdf_sig_mass_frac1_fit"),epdf->Var("Bd_pdf_sig_mass_frac2_fit")));




    epdf->Formula("Bs_signal_pdf_1st_sigma_fit","@0*@1",RooArgList(epdf->Var("Bs_signal_pdf_1st_sigma_mc"),epdf->Var("Bs_scaling_factor_fit")));
    epdf->Formula("Bs_signal_pdf_2nd_sigma_fit","@0*@1",RooArgList(epdf->Var("Bs_signal_pdf_2nd_sigma_mc"),epdf->Var("Bs_scaling_factor_fit")));

    //Bs-Signal
    epdf->CBShape("Bs_pdf_sig_mass_1stCB_fit", epdf->Var("obsMass"),epdf->Var("Bs_signal_pdf_mean_fit"), epdf->Formula("Bs_signal_pdf_1st_sigma_fit"),epdf->Var("Bs_signal_pdf_1st_alpha_fit"), epdf->Var("signal_pdf_n_fit"));
    epdf->CBShape("Bs_pdf_sig_mass_2ndCB_fit", epdf->Var("obsMass"),epdf->Var("Bs_signal_pdf_mean_fit"), epdf->Formula("Bs_signal_pdf_2nd_sigma_fit"),epdf->Var("Bs_signal_pdf_2nd_alpha_fit"), epdf->Var("signal_pdf_n_fit"));
    epdf->CBShape("Bs_pdf_sig_mass_3rdCB_fit", epdf->Var("obsMass"),epdf->Var("Bs_signal_pdf_mean_fit"), epdf->Var("Bs_signal_pdf_3rd_sigma_mc_fit"),epdf->Var("Bs_signal_pdf_3rd_alpha_fit"), epdf->Var("signal_pdf_n_fit"));
  
    epdf->Add("Bs_pdf_sig_mass_3CB_fit", RooArgList(epdf->Pdf("Bs_pdf_sig_mass_1stCB_fit"), epdf->Pdf("Bs_pdf_sig_mass_2ndCB_fit"), epdf->Pdf("Bs_pdf_sig_mass_3rdCB_fit")), RooArgList(epdf->Var("Bs_pdf_sig_mass_frac1_fit"),epdf->Var("Bs_pdf_sig_mass_frac2_fit")));



    //Background
    epdf->Exponential("pdf_bkg_mass_exp_fit", epdf->Var("obsMass"), epdf->Var("background_pdf_lambda_fit"));
    
    //zusammenfuehren
    epdf->Add("pdfExtendFit",RooArgList(epdf->Pdf("Bd_pdf_sig_mass_3CB_fit"),epdf->Pdf("Bs_pdf_sig_mass_3CB_fit"),epdf->Pdf("pdf_bkg_mass_exp_fit")),RooArgList(epdf->Var("Bd_sig_yield"),epdf->Var("Bs_sig_yield"),epdf->Var("bkg_yield")));
    
    

    for(int i=0;i<1000;i++)
    { 
   
    RooDataSet* data = tfac.Generate();
    data->Print(); 
    epdf->Pdf("pdfExtend").getParameters(data)->readFromFile("/home/delten/repos/Toystudies_Master/parameters/massdistribution_BdBs_Gen3CB_Fit3CB_mitBkg_Test-parameter.txt");
    epdf->Pdf("pdfExtend").getParameters(data)->writeToFile(("/home/delten/repos/Toystudies_Master/parameters/massdistribution_BdBs_Gen3CB_Fit3CB_mitBkg_Test-parameter/massdistribution_BdBs_Gen3CB_Fit3CB_mitBkg_Test-parameter.txt.new"+std::to_string(i)).c_str());
    
    /*
     ##########################################   Zum getrennten generieren und fitten #####################
     */
    
    epdf->Pdf("pdfExtendFit").getParameters(data)->readFromFile("/home/delten/repos/Toystudies_Master/parameters/massdistribution_BdBs_Gen3CB_Fit3CB_mitBkg_Test-parameter.txt");
    epdf->Pdf("pdfExtendFit").getParameters(data)->writeToFile(("/home/delten/repos/Toystudies_Master/parameters/massdistribution_BdBs_Gen3CB_Fit3CB_mitBkg_Test_fit-parameter/massdistribution_BdBs_Gen3CB_Fit3CB_mitBkg_Test_fit-parameter.txt.new"+std::to_string(i)).c_str());
    
    RooFitResult* fit_result = epdf->Pdf("pdfExtendFit").fitTo(*data, NumCPU(4), Save(true), Minos(true), Verbose(false), Timer(true), Extended(true),Optimize(0));//,RooFit::Save(true));
    //tstudy.StoreFitResult(fit_result);
    epdf->Pdf("pdfExtendFit").getParameters(data)->writeToFile(("/home/delten/repos/Toystudies_Master/fitresults/fitresults-massdistribution_BdBs_Gen3CB_Fit3CB_mitBkg_Test_fitresult/fitresults-massdistribution_BdBs_Gen3CB_Fit3CB_mitBkg_Test_fitresult.txt"+std::to_string(i)).c_str());
    /*
     #############################################################################################################################################
     */
    
                    
    //print results on terminal
    fit_result->Print();
    TMatrixTSym<double> corMatrix = fit_result->correlationMatrix();
    corMatrix.Print();
    
    //RooArgSets fürs Plotten
    RooArgSet plotobsMass;
    plotobsMass.add(RooArgSet(epdf->Var("obsMass")));
    

    using namespace doofit::plotting;

                    
    cfg_plot.set_plot_directory("/fhgfs/users/delten/DooSoftwareToyStudies/plots/massdistribution_BdBs_Gen3CB_Fit3CB_mitBkg_Test");
    //path for phido when fhgfs is not available
    //cfg_plot.set_plot_directory("/home/delten/toystudies_plot_directory_fhgfs_down_test/massdistribution_3CBGenFitBdsharedBs_Yields_Bkg");
    // plot PDF and directly specify components

    Plot myplot(cfg_plot, epdf->Var("obsMass"), *data, RooArgList(epdf->Pdf("pdfExtendFit")));
    myplot.AddPlotArg(ProjWData(plotobsMass, *data, true));
    myplot.PlotIt();      //plotting command
    
    

    //Zeilen zur Auswertung mit phido (jobs) zur Erstellung einer .root-Datei, auch lokale Überprüfung ob .root-Datei erstellt wurde
    tstudy.StoreFitResult(fit_result);
    }

    tstudy.FinishFitResultSaving();  //works on phido
    tstudy.ReadFitResults();
    tstudy.EvaluateFitResults();
    tstudy.PlotEvaluatedParameters();

    return 0;
    
}
