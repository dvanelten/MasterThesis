#Generation
#SignalGauss
signal_pdf_mean = 5279.61 L(5275 - 5285)
signal_pdf_sigma = 10 L(0 - 20)                

#Fit
#SignalGauss
signal_pdf_mean_fit = 5279.61 L(5275 - 5285)
signal_pdf_sigma_fit = 10 L(0 - 20)


#BkgExponential
#based on notebook fit results (DataSidebandFit)
#background_pdf_lambda = -0.0013738752393628317 C L(-0.01 - -0.0000001)
#background_pdf_lambda_fit = -0.0013738752393628317 L(-0.01 - -0.0000001)
#lambda_fit_result_data = -0.0013738752393628317
#sideband_fit_result_data = -0.001251608136387361

#bkgyield tests
#background_pdf_lambda = 0 C L(-0.01 - 0.01)
#background_pdf_lambda_fit = 0 L(-0.01 - 0.01)
background_pdf_lambda = -0.0013738752393628317 C L(-0.1 - -0.00000000001)
background_pdf_lambda_fit = -0.0013738752393628317 L(-0.1 - -0.00000000001)


sig_yield = 1633 L (-INF - +INF)
bkg_yield = 5150 L (-INF - +INF)

