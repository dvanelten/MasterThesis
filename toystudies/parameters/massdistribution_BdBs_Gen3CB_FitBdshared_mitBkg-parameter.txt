#GenerationSignalModel

#Bd GenerationModel
Bd_signal_pdf_mean = 5279.852105950422 C L(5275 - 5285)

Bd_signal_pdf_1st_sigma_mc = 9.152486153267601 C L(0 - 15)
Bd_signal_pdf_2nd_sigma_mc = 8.567911230163581 C L(5 - 15)
Bd_signal_pdf_3rd_sigma_mc = 13.751033040426107 C L(10 - 25)

Bd_signal_pdf_1st_alpha = 1.10058298010218 C L(0 - 2)
Bd_signal_pdf_2nd_alpha = -1.0644837971312557 C L(-2 - 0)
Bd_signal_pdf_3rd_alpha = 0.12559143681661872 C L(0 - 0.5)

Bd_signal_pdf_1st_n = 10 C L(5 - 15)
Bd_signal_pdf_2nd_n = 10 C L(5 - 15)
Bd_signal_pdf_3rd_n = 10 C L(5 - 15)

Bd_pdf_sig_mass_frac1 = 0.5912360627703275 C L(0.2 - 0.8)
Bd_pdf_sig_mass_frac2 = 0.3962949887654593 C L(0.2 - 0.8)



#Bs GenerationModel
Bs_signal_pdf_mean = 5367.051625624893 C L(5330 - 5410)

Bs_signal_pdf_1st_sigma_mc = 9.201896765251663 C L(0 - 15)
Bs_signal_pdf_2nd_sigma_mc = 9.120266163859688 C L(5 - 15)
Bs_signal_pdf_3rd_sigma_mc = 12.782648204717908 C L(10 - 25)

Bs_signal_pdf_1st_alpha = 1.050501336583639 C L(0 - 5)
Bs_signal_pdf_2nd_alpha = -1.15351433678721 C L(-5 - 0)
Bs_signal_pdf_3rd_alpha = 0.1347146394943382 C L(0 - 0.5)

Bs_signal_pdf_1st_n = 10 C L(5 - 15)
Bs_signal_pdf_2nd_n = 10 C L(5 - 15)
Bs_signal_pdf_3rd_n = 10 C L(5 - 15)

#3CB as TripleCB
Bs_pdf_sig_mass_frac1 = 0.5297286627808918 C L(0.5 - 0.7)
Bs_pdf_sig_mass_frac2 = 0.4575989947276767 C L(0.3 - 0.5)



#FitSignalModel
#mean
Bd_signal_pdf_mean_fit = 5279.852105950422 L(5275 - 5285)
mean_difference = 87.19951967 L (80 - 100)

#sigma
Bd_signal_pdf_3rd_sigma_mc_fit = 13.751033040426107 C L(10 - 25)
Bd_scaling_factor_fit = 1 L(0.5 - 2.0)
#alpha
Bd_signal_pdf_1st_alpha_fit = 1.10058298010218 C L(0 - 2)
Bd_signal_pdf_2nd_alpha_fit = -1.0644837971312557 C L(-2 - 0)
Bd_signal_pdf_3rd_alpha_fit = 0.12559143681661872 C L(0 - 0.5)
#n
signal_pdf_n_fit = 10 C L(5 - 15)
#frac
Bd_pdf_sig_mass_frac1_fit = 0.5912360627703275 C L(0.2 - 0.8)
Bd_pdf_sig_mass_frac2_fit = 0.3962949887654593 C L(0.2 - 0.8)

#BkgExponential
#based on notebook fit results (DataSidebandFit)
background_pdf_lambda = -0.0013738752393628317 C L(-0.01 - -0.0000001)
background_pdf_lambda_fit = -0.0013738752393628317 L(-0.01 - -0.0000001)

#Yields
Bd_sig_yield = 1633 L (-INF - +INF)
#Bs: sig_yield(datafit)*fragmentation_factor*scaling_factor
#1633*0.259*1.068
#Bs_sig_yield = 452 L (-INF - +INF)
bkg_yield = 5150 L (-INF - +INF)

branching_factor = 1 L (0.0 - 2.0)
fragmentation_factor = 0.259 C L(0.05 - 0.5)
scaling_factor_run2 = 1.068 C L(0.1 - 2.0)
