#GenerationSignalModel
#3CB
#based on notebook settings (DataFit)
signal_pdf_mean = 5279.852105950422 L(5275 - 5285)

signal_pdf_1st_sigma = 9.117204761212278 L(0 - 15)
signal_pdf_2nd_sigma = 9.651246792835284 L(5 - 15)
signal_pdf_3rd_sigma = 18.002014357315236 L(10 - 25)

signal_pdf_1st_alpha = 1.2200777258574458 C L(0 - 5)
signal_pdf_2nd_alpha = -1.2990586885063573 C L(-5 - 0)
signal_pdf_3rd_alpha = 0.15656331022892378 C L(0 - 0.5)

signal_pdf_1st_n = 10 C L(5 - 15)
signal_pdf_2nd_n = 10 C L(5 - 15)
signal_pdf_3rd_n = 10 C L(5 - 15)


#3CB as TripleCB
pdf_sig_mass_frac1 = 0.5767727053120193 C L(0.5 - 0.7)
pdf_sig_mass_frac2 = 0.4116113516422507 C L(0.3 - 0.5)


#FitSignalModel
signal_pdf_mean_fit = 5279.852105950422 L(5275 - 5285)

signal_pdf_1st_sigma_mc_fit = 9.117204761212278 C L(0 - 15)
signal_pdf_2nd_sigma_mc_fit = 9.6512467928355284 C L(0 - 20)
scaling_factor = 1 L(0.1 - 2)


signal_pdf_1st_alpha_fit = 1.2200777258574458 C L(0 - 5)
signal_pdf_2nd_alpha_fit = -1.2990586885063573 C L(-5 - 0)

signal_pdf_1st_n_fit = 10 C L(5 - 15)
signal_pdf_2nd_n_fit = 10 C L(5 - 15)

pdf_sig_mass_frac_fit = 0.5 L(0.1 - 0.9)


#BkgExponential
#based on notebook fit results (DataSidebandFit)
background_pdf_lambda = -0.0013738752393628317 C L(-0.01 - -0.0000001)
background_pdf_lambda_fit = -0.0013738752393628317 L(-0.01 - -0.0000001)
#lambda_fit_result_data = -0.0013738752393628317
#sideband_fit_result_data = -0.001251608136387361

#Yields
sig_yield = 1633 L (-INF - +INF)
bkg_yield = 5150 L (-INF - +INF)
