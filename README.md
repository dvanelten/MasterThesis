# MasterThesis 2017/2018

This project contains the programs written during the master thesis 2017/2018 at the TU Dortmund. 
The target is the determination of a branching ratio of the decay B(s)2DD at the LHCb Experiment.
For this, statistical tools for Big Data analysis were used (python: matplotlib, pandas, sklearn, seaborn; CERN: (py)ROOT, TMVA).

## Subprojects


- `Data_processing`: processing data from the LHCb DiracPortal and subsequent selection
- `Jupyter_Notebooks`: mass models, BDT, figure of merit
- `Toystudies`: studies on stability, systematic errors
- `tmva`: boosted decision trees created by TMVA


## Linked projects

- https://git.e5.physik.tu-dortmund.de/lhcbanalysis
- https://git.e5.physik.tu-dortmund.de/lhcbanalysis/dopy
- https://git.e5.physik.tu-dortmund.de/lhcbanalysis/tuplemaker
- https://git.e5.physik.tu-dortmund.de/lhcbdoosoftware
