import os

#cmdline = "hadd -f /fhgfs/users/delten/data/2016_stripping28/B02DD_MU_data_2016stripping28.root"
#cmdline = "hadd -f /fhgfs/users/delten/mc/Bs2DD/2015/Bs2DD_MD_2015_MC.root"
cmdline = "hadd -f /fhgfs/users/delten/mc/Lb2LcD/2016/Lb2LcD_MD_2016_MC.root"

datasets = {55:8}                    #jobNr:NrOfSubjobs
for dset in datasets.iterkeys():
	#print dset , datasets[dset] 

	#INPUTDIR ='/fhgfs/users/delten/gangadir/workspace/delten/LocalXML/'+str(dset)+'/'
        INPUTDIR ='/home/delten/gangadir/workspace/delten/LocalXML/'+str(dset)+'/' 

	files = map(lambda n: INPUTDIR + str(n) + '/output/DTT.root', range(0, datasets[dset], 1))
	missing = filter(lambda x: not os.path.exists(x), files)
	if missing:
		print("Missing files: {}".format(missing))



	tmpfiles = []
	not_missing = filter(lambda y: os.path.exists(y), files)
	#for i in range((len(files)//100)+1):
	for i in range((len(not_missing)//100)+1):
		#tmpfile = "/fhgfs/users/delten/data/2016_stripping28/DTT_tmp_{}.root".format(i)
		#tmpfile = "/fhgfs/users/delten/mc/Bs2DD/2015/DTT_tmp_{}.root".format(i)
                tmpfile = "/fhgfs/users/delten/mc/Lb2LcD/2016/DTT_tmp_{}.root".format(i)

                tmpfiles.append(tmpfile)
		stop=(i+1)*100
		if i==len(not_missing)//100:
			stop = len(not_missing)
			#print("hadd -f {} {}".format(tmpfile, " ".join(not_missing[i*100:stop])))
		os.system("hadd -f {} {}".format(tmpfile, " ".join(not_missing[i*100:stop])))

	set_file = "/fhgfs/users/delten/backup/DTT_"+str(dset)+".root"
	#print("hadd -f {} {}".format(set_file, " ".join(tmpfiles)))
	os.system("hadd -f {} {}".format(set_file, " ".join(tmpfiles)))
	cmdline += " "+set_file
	##print cmdline
	print 'Merging'+str(dset)+' done.'

print cmdline
os.system(cmdline)
print 'Merging completed'


