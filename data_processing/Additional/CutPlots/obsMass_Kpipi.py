
import ROOT as r
import numpy as np

from ROOT import gROOT, TCanvas, TPad, TF1, TFile, TTree, gRandom, TH1F, gStyle, gPad, TLine, TH1D, TH1

from ROOT import RooRealVar, RooFormulaVar, RooVoigtian, RooChebychev, RooArgList, \
                 RooArgSet, RooAddPdf, RooDataSet, RooDataHist, RooCategory, RooSimultaneous, \
                 RooBreitWigner, RooCBShape, RooFFTConvPdf, RooGaussian,RooExponential, \
                 RooBinning, kRed, kBlue, kDotted,TString,RooAbsData, RooPlot, TCut, RooAbsData

import os, sys, time, random

import ROOT
from ROOT import TTree, TFile

from ROOT import RooRealVar,RooCBShape,RooGaussian,RooExponential,RooArgSet, RooAddPdf, RooArgList,RooFormulaVar,RooDataSet, RooAbsData
from ROOT import kRed, kBlue, kGreen, kYellow, kBlack, kDotted

# from root_numpy import root2array, rec2array, array2root

import pandas as pd
import numpy as np
import scipy 
import root_pandas as rp


sys.path.append('/net/nfshome/home/delten/repos/root_utils/')
sys.path.append('/net/nfshome/home/delten/repos/root_numpy/')
sys.path.append('/net/nfshome/home/delten/repos')
import root_numpy as ry

# Set LHCb plotting style
from dopy.doroot.lhcb_style import set_lhcb_style
set_lhcb_style()

# Enable plotting in notebooks

# Import plotting functions and data reader from root_utils
from dopy.doroot.root_utils import (build_tchain_from_files, read_roodataset_from_tree,
                                    plot_simple, plot_pulls)

import sys
sys.path.append('/net/nfshome/home/delten/repos/root_utils/')

gStyle.SetPadLeftMargin(0.2)
gStyle.SetPadRightMargin(0.1)

#------------------------------------config------------------------------

######Plotting BDT classifier with MC and sweighted data#####
prefix_data = "/fhgfs/users/delten/data/KKpi/efficiencies/";
#filename_data = "data20152016stripping28_full_grimreaper_KKpi_flat_idxPV_LoKi_bscut_sanitycuts.root" #sanitycuts
#filename_data ='data20152016stripping28_full_grimreaper_KKpi_flat_idxPV_LoKi_bscut_sanitycuts_neupres_Mflat50MeV_allcuts.root' #preselections
filename_data ='data20152016stripping28_full_grimreaper_KKpi_flat_idxPV_LoKi_bscut_sanitycuts_neupres_Mflat50MeV_allcuts_allVetos.root' #vetos

file_data= r.TFile(prefix_data+filename_data, "READ")
tree_data = file_data.Get("DecayTree")

file_mc = r.TFile(prefix_data+filename_data, "READ")
tree_mc = file_mc.Get("DecayTree")

tree_data.GetEntries()

#define histogramnames
histo_data = "hist"
histo_mc = "hist"
histo = "hist"
#define the canvas
can = r.TCanvas("can","can",1200,900);
#defining legend
leg = r.TLegend(0.57,0.75,0.87,0.88);
leg.SetFillStyle(0)

####histogram definition

##Mflat-Cut-Sanity
#hist_data = r.TH1D(histo_data,histo_data,100,1800,2000)
#entries_data = tree_data.Draw("B0_FitPVConst_Dplus_M_flat"+">>"+histo_data, "")
hist_mc = r.TH1D(histo_data,histo_data,150,5000,6000)
entries_mc = tree_mc.Draw("obsMass"+">>"+histo_data, "")

#hist_data.SetFillColor(13)
hist_mc.SetFillColor(46)

#Mflat-Cut
hist_mc.GetXaxis().SetTitle("#it{m_{D^{+}D^{-}}} (MeV/c^{2})")
hist_mc.GetYaxis().SetTitle("Kandidaten / (10,0 MeV/c^{2})")

hist_mc.GetXaxis().SetTitleSize(0.069)
hist_mc.GetYaxis().SetTitleSize(0.069)
hist_mc.GetXaxis().SetTitleOffset(1.05)
hist_mc.GetYaxis().SetTitleOffset(1.1)
#hist_mc.SetFillStyle(3018)
hist_mc.GetYaxis().SetLabelSize(0.069)
hist_mc.GetXaxis().SetLabelSize(0.069)


leg.AddEntry(0,"LHCb inoffiziell", "")
leg.AddEntry(0,"#it{L}_{int} = 2 fb^{-1}","")
leg.Draw()

gPad.Update()
gPad.RedrawAxis()

#style options
r.gStyle.SetOptTitle(0)
r.gStyle.SetOptStat(0)

#can.SaveAs('~/obsMassSanityKKpi.pdf')
#can.SaveAs('~/obsMassPreselectionsKKpi.pdf')
can.SaveAs('~/obsMassVetosKKpi.pdf')
can.Clear()
leg.Clear()


