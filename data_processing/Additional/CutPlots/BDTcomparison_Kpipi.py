
import ROOT as r
import numpy as np

from ROOT import gROOT, TCanvas, TPad, TF1, TFile, TTree, gRandom, TH1F, gStyle, gPad, TLine, TH1D, TH1

from ROOT import RooRealVar, RooFormulaVar, RooVoigtian, RooChebychev, RooArgList, \
                 RooArgSet, RooAddPdf, RooDataSet, RooDataHist, RooCategory, RooSimultaneous, \
                 RooBreitWigner, RooCBShape, RooFFTConvPdf, RooGaussian,RooExponential, \
                 RooBinning, kRed, kBlue, kDotted,TString,RooAbsData, RooPlot, TCut, RooAbsData

import os, sys, time, random

import ROOT
from ROOT import TTree, TFile

from ROOT import RooRealVar,RooCBShape,RooGaussian,RooExponential,RooArgSet, RooAddPdf, RooArgList,RooFormulaVar,RooDataSet, RooAbsData
from ROOT import kRed, kBlue, kGreen, kYellow, kBlack, kDotted

# from root_numpy import root2array, rec2array, array2root

import pandas as pd
import numpy as np
import scipy 
import root_pandas as rp


sys.path.append('/net/nfshome/home/delten/repos/root_utils/')
sys.path.append('/net/nfshome/home/delten/repos/root_numpy/')
sys.path.append('/net/nfshome/home/delten/repos')
import root_numpy as ry

# Set LHCb plotting style
from dopy.doroot.lhcb_style import set_lhcb_style
set_lhcb_style()

# Enable plotting in notebooks

# Import plotting functions and data reader from root_utils
from dopy.doroot.root_utils import (build_tchain_from_files, read_roodataset_from_tree,
                                    plot_simple, plot_pulls)

import sys
sys.path.append('/net/nfshome/home/delten/repos/root_utils/')

gStyle.SetPadLeftMargin(0.2)
gStyle.SetPadRightMargin(0.1)

#------------------------------------config------------------------------

######Plotting BDT classifier with MC and sweighted data#####
prefix_data = "/fhgfs/users/delten/data/Kpipi/efficiencies/TMVA/";
filename_data = "data20152016stripping28_full_grimreaper_Kpipi_flat_idxPV_bscut_sanitycuts_neupres_Mflat50MeV_allcuts_neuvet_allcuts_massfitcut_sweights_BDTVariables__BDT_response_sweights.root"

prefix_mc = "/fhgfs/users/delten/mc/B02DD/Kpipi/TMVA/";
filename_mc = "B02DD_20152016combined_MC_full_grimreaper_flat_Kpipi_idx_bkg_sanitycuts_neupres_Mflat50MeV_allcuts_neuvet_allcuts_BDTVariables__BDT_response_massfitcut.root";

file_data= r.TFile(prefix_data+filename_data, "READ")
tree_data = file_data.Get("DecayTree")

file_mc = r.TFile(prefix_mc+filename_mc, "READ")
tree_mc = file_mc.Get("DecayTree")

tree_data.GetEntries()

#define histogramnames
histo_data = "hist"
histo_mc = "hist"
histo = "hist"
#define the canvas
can = r.TCanvas("can","can",1200,900);
#defining legend
leg = r.TLegend(0.2,0.7,0.6,0.88);
#leg = r.TLegend(0.57,0.75,0.87,0.88);
leg.SetFillStyle(0)


####histogram definition
#hist_data = r.TH1D(histo_data, histo_data, 100, -5 , 5)
hist_data = r.TH1D(histo_data,histo_data,100,-1,1)

entries_data = tree_data.Draw("BDT_classifier"+">>"+histo_data, "SigYield_sw", "norm")

#hist_mc = r.TH1D(histo_mc, histo_mc, 100, -5 , 5)
hist_mc = r.TH1D(histo_mc,histo_mc,100,-1,1)
hist_mc.Sumw2()
entries_mc = tree_mc.Draw("BDT_classifier"+">>"+histo_mc,"", "enormsame")
hist_mc.SetLineColor(2)
hist_mc.SetMarkerColor(2)
hist_mc.SetTitleSize(0.07)
hist_mc.GetXaxis().SetTitle("BDT Klassifizierer")
hist_data.GetXaxis().SetTitle("BDT Klassifizierer")
hist_mc.GetYaxis().SetTitle("willk. Einh.")
hist_data.GetYaxis().SetTitle("willk. Einh.")
hist_mc.GetXaxis().SetTitleSize(0.07)
hist_data.GetXaxis().SetTitleSize(0.07)



leg.AddEntry(0,"LHCb inoffiziell", "")
leg.AddEntry(hist_data,"gewichtete Daten","ep")
leg.AddEntry(hist_mc,"simulierte Daten","ep")
leg.SetTextSize(0.05)
leg.Draw()

gPad.Update()
gPad.RedrawAxis()

#style options
r.gStyle.SetOptTitle(0)
r.gStyle.SetOptStat(0)

can.SaveAs('~/BDT.pdf')
can.Clear()
leg.Clear()


###Plotting the difference of MC and sweighted data###
###drawing hist and legend
#hist_data = r.TH1D(histo_data, histo_data, 20, -5 , 5)
hist_data = r.TH1D(histo_data,histo_data,20,-1,1)
entries_data = tree_data.Draw("BDT_classifier"+">>"+histo_data, "SigYield_sw", "")


#hist_mc = r.TH1D(histo_mc, histo_mc, 20, -5 , 5)
hist_mc = r.TH1D(histo_mc,histo_mc,20,-1,1)
entries_mc = tree_mc.Draw("BDT_classifier"+">>"+histo_mc, "", "")

hist_data.Sumw2()
hist_data.Scale(1/hist_data.GetSumOfWeights())
hist_mc.Sumw2()
hist_mc.Scale(1/entries_mc)

hist_mc.Add(hist_data, -1.)
#hist_data.Draw("e")
hist_mc.Draw("e")

#entries_mc = tree_mc.Draw("BDT_classifier"+">>"+histo_mc, "", "")

hist_mc.GetXaxis().SetTitle("Residuum des BDT Klassifizierers")

leg.AddEntry(0,"LHCb inoffiziell", "")
leg.Draw()

gPad.Update()
gPad.RedrawAxis()

#style options
r.gStyle.SetOptTitle(0)
r.gStyle.SetOptStat(0)

can.SaveAs('~/residual_BDT.pdf')
can.Clear()
leg.Clear()
