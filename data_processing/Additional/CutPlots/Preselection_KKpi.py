
import ROOT as r
import numpy as np

from ROOT import gROOT, TCanvas, TPad, TF1, TFile, TTree, gRandom, TH1F, gStyle, gPad, TLine, TH1D, TH1

from ROOT import RooRealVar, RooFormulaVar, RooVoigtian, RooChebychev, RooArgList, \
                 RooArgSet, RooAddPdf, RooDataSet, RooDataHist, RooCategory, RooSimultaneous, \
                 RooBreitWigner, RooCBShape, RooFFTConvPdf, RooGaussian,RooExponential, \
                 RooBinning, kRed, kBlue, kDotted,TString,RooAbsData, RooPlot, TCut, RooAbsData

import os, sys, time, random

import ROOT
from ROOT import TTree, TFile

from ROOT import RooRealVar,RooCBShape,RooGaussian,RooExponential,RooArgSet, RooAddPdf, RooArgList,RooFormulaVar,RooDataSet, RooAbsData
from ROOT import kRed, kBlue, kGreen, kYellow, kBlack, kDotted

# from root_numpy import root2array, rec2array, array2root

import pandas as pd
import numpy as np
import scipy 
import root_pandas as rp


sys.path.append('/net/nfshome/home/delten/repos/root_utils/')
sys.path.append('/net/nfshome/home/delten/repos/root_numpy/')
sys.path.append('/net/nfshome/home/delten/repos')
import root_numpy as ry

# Set LHCb plotting style
from dopy.doroot.lhcb_style import set_lhcb_style
set_lhcb_style()

# Enable plotting in notebooks

# Import plotting functions and data reader from root_utils
from dopy.doroot.root_utils import (build_tchain_from_files, read_roodataset_from_tree,
                                    plot_simple, plot_pulls)

import sys
sys.path.append('/net/nfshome/home/delten/repos/root_utils/')

gStyle.SetPadLeftMargin(0.2)
gStyle.SetPadRightMargin(0.1)

#------------------------------------config------------------------------

######Plotting BDT classifier with MC and sweighted data#####
prefix_data = "/fhgfs/users/delten/data/KKpi/efficiencies/Einzeleffizienzen_Preselection/";
#filename_data = "data20152016stripping28_full_grimreaper_KKpi_flat_idxPV_LoKi_bscut_sanitycuts.root" #sanitycuts
filename_data = "data20152016stripping28_full_grimreaper_KKpi_flat_idxPV_LoKi_bscut_sanitycuts_neupres_Mflat50MeV.root"

file_data= r.TFile(prefix_data+filename_data, "READ")
tree_data = file_data.Get("DecayTree")

file_mc = r.TFile(prefix_data+filename_data, "READ")
tree_mc = file_mc.Get("DecayTree")

tree_data.GetEntries()

#define histogramnames
histo_data = "hist"
histo_mc = "hist"
histo = "hist"
#define the canvas
can = r.TCanvas("can","can",1200,900);
#defining legend
leg = r.TLegend(0.57,0.75,0.87,0.88);
leg.SetFillStyle(0)
leg.SetFillStyle(0)
####histogram definition

##Mflat-Cut-Sanity
#hist_data = r.TH1D(histo_data,histo_data,100,1800,2000)
#entries_data = tree_data.Draw("B0_FitPVConst_Dplus_M_flat"+">>"+histo_data, "")
#hist_mc = r.TH1D(histo_mc,histo_mc,100,1800,2000)
#entries_mc = tree_mc.Draw("B0_FitPVConst_Dplus_M_flat"+">>"+histo_data,"(abs(B0_FitPVConst_Dplus_M_flat-1869.61)<50)", "same")

##Mflat-Cut
#hist_data = r.TH1D(histo_data,histo_data,121,1810,1930)
#entries_data = tree_data.Draw("B0_FitPVConst_Dplus_M_flat"+">>"+histo_data, "")
#hist_mc = r.TH1D(histo_mc,histo_mc,121,1810,1930)
#entries_mc = tree_mc.Draw("B0_FitPVConst_Dplus_M_flat"+">>"+histo_data,"(abs(B0_FitPVConst_Dplus_M_flat-1869.61)<45)", "same")

##Mflat-Cut-SanityPreselections
#hist_data = r.TH1D(histo_data,histo_data,100,1800,2000)
#entries_data = tree_data.Draw("B0_FitPVConst_Dplus_M_flat"+">>"+histo_data, "")
#hist_mc = r.TH1D(histo_mc,histo_mc,100,1800,2000)
#entries_mc = tree_mc.Draw("B0_FitPVConst_Dplus_M_flat"+">>"+histo_data,"(abs(B0_FitPVConst_Dplus_M_flat-1869.61)<45)", "same")

##TauSignificance-Cut
#hist_data = r.TH1D(histo_data,histo_data,100,-10,20)
#entries_data = tree_data.Draw("varDplusTauSignificance"+">>"+histo_data, "")
#hist_mc = r.TH1D(histo_mc,histo_mc,100,-10,20)
#entries_mc = tree_mc.Draw("varDplusTauSignificance"+">>"+histo_data,"varDplusTauSignificance>-1.3", "same")

#Chi2-Cut
hist_data = r.TH1D(histo_data,histo_data,100,0,200)
entries_data = tree_data.Draw("B0_FitDDConst_chi2_flat"+">>"+histo_data, "")
hist_mc = r.TH1D(histo_mc,histo_mc,100,0,200)
entries_mc = tree_mc.Draw("B0_FitDDConst_chi2_flat"+">>"+histo_data,"B0_FitDDConst_chi2_flat<55", "same")


hist_data.SetFillColor(13)
hist_mc.SetFillColor(46)

##Mflat-Cut
#hist_data.GetXaxis().SetTitle("#it{m_{D^{#pm}}} (MeV/c^{2})")
#hist_data.GetYaxis().SetTitle("Kandidaten / (2,0 MeV/c^{2})")

##TauSignificance-Cut
#hist_data.GetXaxis().SetTitle("#it{#frac{#tau}{#sigma_{#tau}} (D^{#pm})}")
#hist_data.GetYaxis().SetTitle("Kandidaten / 0,4")

#Chi2-Cut
hist_data.GetXaxis().SetTitle("#it{#chi^{2} (m_{D}} bschr.)")
hist_data.GetYaxis().SetTitle("Kandidaten / 2,0")

hist_data.GetXaxis().SetTitleSize(0.069)
hist_data.GetYaxis().SetTitleSize(0.069)
hist_data.GetXaxis().SetTitleOffset(1.05)
#hist_data.GetYaxis().SetTitleOffset(1.2)
hist_data.GetYaxis().SetTitleOffset(1.35)    #for Tau and Chi2
hist_data.SetFillStyle(3018)
hist_data.GetYaxis().SetLabelSize(0.069)
hist_data.GetXaxis().SetLabelSize(0.069)

leg.AddEntry(0,"LHCb inoffiziell", "")
leg.AddEntry(0,"#it{L}_{int} = 2 fb^{-1}","")
leg.Draw()

gPad.Update()
gPad.RedrawAxis()

#style options
r.gStyle.SetOptTitle(0)
r.gStyle.SetOptStat(0)

#can.SaveAs('~/SanityMCutKKpi.pdf')
#can.SaveAs('~/PreselectionsMCutKKpi.pdf')
#can.SaveAs('~/SanityPreselectionsMCutKKpi.pdf')
#can.SaveAs('~/PreselectionsTauSignCutKKpi.pdf')
can.SaveAs('~/PreselectionsChi2CutKKpi.pdf')
can.Clear()
leg.Clear()


