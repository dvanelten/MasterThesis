
import ROOT as r
import numpy as np

from ROOT import gROOT, TCanvas, TPad, TF1, TFile, TTree, gRandom, TH1F, gStyle, gPad, TLine, TH1D, TH1

from ROOT import RooRealVar, RooFormulaVar, RooVoigtian, RooChebychev, RooArgList, \
                 RooArgSet, RooAddPdf, RooDataSet, RooDataHist, RooCategory, RooSimultaneous, \
                 RooBreitWigner, RooCBShape, RooFFTConvPdf, RooGaussian,RooExponential, \
                 RooBinning, kRed, kBlue, kDotted,TString,RooAbsData, RooPlot, TCut, RooAbsData

import os, sys, time, random

import ROOT
from ROOT import TTree, TFile

from ROOT import RooRealVar,RooCBShape,RooGaussian,RooExponential,RooArgSet, RooAddPdf, RooArgList,RooFormulaVar,RooDataSet, RooAbsData
from ROOT import kRed, kBlue, kGreen, kYellow, kBlack, kDotted

# from root_numpy import root2array, rec2array, array2root

import pandas as pd
import numpy as np
import scipy 
import root_pandas as rp


sys.path.append('/net/nfshome/home/delten/repos/root_utils/')
sys.path.append('/net/nfshome/home/delten/repos/root_numpy/')
sys.path.append('/net/nfshome/home/delten/repos')
import root_numpy as ry

# Set LHCb plotting style
from dopy.doroot.lhcb_style import set_lhcb_style
set_lhcb_style()

# Enable plotting in notebooks

# Import plotting functions and data reader from root_utils
from dopy.doroot.root_utils import (build_tchain_from_files, read_roodataset_from_tree,
                                    plot_simple, plot_pulls)

import sys
sys.path.append('/net/nfshome/home/delten/repos/root_utils/')

gStyle.SetPadLeftMargin(0.2)
gStyle.SetPadRightMargin(0.1)

#------------------------------------config------------------------------

######Plotting BDT classifier with MC and sweighted data#####
prefix_data = "/fhgfs/users/delten/data/KKpi/efficiencies/";
filename_data = "data20152016stripping28_full_grimreaper_KKpi_flat_idxPV_LoKi_bscut_sanitycuts_neupres_Mflat50MeV_allcuts.root"

file_data= r.TFile(prefix_data+filename_data, "READ")
tree_data = file_data.Get("DecayTree")

file_mc = r.TFile(prefix_data+filename_data, "READ")
tree_mc = file_mc.Get("DecayTree")

tree_data.GetEntries()

#define histogramnames
histo_data = "hist"
histo_mc = "hist"
histo = "hist"
#define the canvas
can = r.TCanvas("can","can",1200,900);
#defining legend
leg = r.TLegend(0.57,0.75,0.87,0.88);
leg.SetFillStyle(0)


####histogram definition

##Phi-Veto
#hist_data = r.TH1D(histo_data,histo_data,100,950,1200)
#entries_data = tree_data.Draw("varPhiMassHypoforD2KKpi"+">>"+histo_data, "")
#hist_mc = r.TH1D(histo_mc,histo_mc,100,950,1200)
#entries_mc = tree_mc.Draw("varPhiMassHypoforD2KKpi"+">>"+histo_data,"(abs(varPhiMassHypoforD2KKpi-1019.455)>10)", "same")

##Ds-Veto
#hist_data = r.TH1D(histo_data,histo_data,100,1800,2200)
#entries_data = tree_data.Draw("varMassHypoforD2KKpi_KpiK_LoKi"+">>"+histo_data, "")
#hist_mc = r.TH1D(histo_mc,histo_mc,100,1800,2200)
#entries_mc = tree_mc.Draw("varMassHypoforD2KKpi_KpiK_LoKi"+">>"+histo_data,"((abs(varMassHypoforD2KKpi_KpiK_LoKi-1969.0)>25))", "same")

##LcSingle-Veto
#hist_data = r.TH1D(histo_data,histo_data,100,2000,2800)
#entries_data = tree_data.Draw("varLcMassHypoforD2KKpi_Kppi"+">>"+histo_data, "")
#hist_mc = r.TH1D(histo_mc,histo_mc,100,2000,2800)
#entries_mc = tree_data.Draw("varLcMassHypoforD2KKpi_Kppi"+">>"+histo_data, "(abs(varLcMassHypoforD2KKpi_Kppi-2286.46)>25||(varD_KKpi_KSC_ProbNNp/(varD_KKpi_KSC_ProbNNk+varD_KKpi_KSC_ProbNNp))<0.41)","same") 

##LcDouble-Veto
#hist_data = r.TH1D(histo_data,histo_data,100,2000,2800)
#entries_data = tree_data.Draw("varLcMassHypoforD2KKpi_Kpip"+">>"+histo_data, "")
#hist_mc = r.TH1D(histo_mc,histo_mc,100,2000,2800)
#entries_mc = tree_data.Draw("varLcMassHypoforD2KKpi_Kpip"+">>"+histo_data, "(abs(varLcMassHypoforD2KKpi_Kpip-2286.4)>25||(((varD_KKpi_KSC_ProbNNpi/(varD_KKpi_KSC_ProbNNk+varD_KKpi_KSC_ProbNNpi))<0.55)&&(varD_KKpi_piSC_ProbNNp/(varD_KKpi_piSC_ProbNNp+varD_KKpi_piSC_ProbNNpi))<0.55))","same")

#SingleCharm-Veto
hist_data = r.TH1D(histo_data,histo_data,100,5000,5500)
entries_data = tree_data.Draw("varBMassHypo_Dpluspipipi"+">>"+histo_data, "")
hist_mc = r.TH1D(histo_mc,histo_mc,100,5000,5500)
entries_mc = tree_mc.Draw("varBMassHypo_Dpluspipipi"+">>"+histo_data,"varDplusBVtxSepChi2>5&&varDminusBVtxSepChi2>5", "same")

hist_data.SetFillColor(13)
hist_mc.SetFillColor(46)

##Phi-Veto
#hist_data.GetXaxis().SetTitle("#it{m_{KK}} (MeV/c^{2})")
#hist_data.GetYaxis().SetTitle("Kandidaten / (2,5 MeV/c^{2})")

##Ds-Veto
#hist_data.GetXaxis().SetTitle("#it{m_{K#piK}} (MeV/c^{2})")
#hist_data.GetYaxis().SetTitle("Kandidaten / (4,0 MeV/c^{2})")

##LcSingle-Veto
#hist_data.GetXaxis().SetTitle("#it{m_{Kp#pi}} (MeV/c^{2})")
#hist_data.GetYaxis().SetTitle("Kandidaten / (8,0 MeV/c^{2})")

##LcDouble-Veto
#hist_data.GetXaxis().SetTitle("#it{m_{K#pip}} (MeV/c^{2})")
#hist_data.GetYaxis().SetTitle("Kandidaten / (8,0 MeV/c^{2})")

#SingleCharm-Veto
hist_data.GetXaxis().SetTitle("#it{m_{D#pi#pi#pi}} (MeV/c^{2})")
hist_data.GetYaxis().SetTitle("Kandidaten / (5,0 MeV/c^{2})")

hist_data.GetXaxis().SetTitleSize(0.069)
hist_data.GetYaxis().SetTitleSize(0.069)
hist_data.GetXaxis().SetTitleOffset(1.1)
hist_data.GetYaxis().SetTitleOffset(1.2)
hist_data.SetFillStyle(3018)
hist_data.GetYaxis().SetLabelSize(0.069)
hist_data.GetXaxis().SetLabelSize(0.069)
#setting for phi-veto KKpi
#hist_data.GetYaxis().SetRangeUser(0,1600)

leg.AddEntry(0,"LHCb inoffiziell", "")
leg.AddEntry(0,"#it{L}_{int} = 2 fb^{-1}","")
leg.SetFillStyle(0)
leg.Draw()

gPad.Update()
gPad.RedrawAxis()

#style options
r.gStyle.SetOptTitle(0)
r.gStyle.SetOptStat(0)

#can.SaveAs('~/PhiVetoKKpi.pdf')
#can.SaveAs('~/DsVetoKKpi.pdf')
#can.SaveAs('~/LcVetoSingleKKpi.pdf')
#can.SaveAs('~/LcVetoDoubleKKpi.pdf')
can.SaveAs('~/SingleCharmKKpi.pdf')
can.Clear()
leg.Clear()


