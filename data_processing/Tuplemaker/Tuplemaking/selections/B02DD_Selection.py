"""
 Script to create a DecayTreeTuple for B02DD analysis with 2015/2016 data:
"""
__author__ = "Dennis van Elten <dennis.vanelten@tu-dortmund.de>"



from Configurables import DaVinci


update_track_ghostprob = False 


max_events = -1
skip_events = -1



################################ SIM YES/NO ###############################################

prefix = ''
if DaVinci().Simulation == True:
  #prefix = 'B2DD.Strip/'
  prefix = 'B2DD_Lb2LcD.Strip/' #Lb BKG MC
else:
  #prefix = 'BhadronCompleteEvent/' #stripping28
  rootInTes = '/Event/Bhadron'
  DaVinci().RootInTES = rootInTes

################################# IMPORTS ##################################################

import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *
from DecayTreeTuple.Configuration import *
from Configurables import (BDecayTool,
                           BTagging,
                           BTaggingTool,
                           CheckPV,
                           CombineParticles,
                           DecayTreeTuple,
                           EventTuple,
                           FilterDesktop,
                           FitDecayTrees,
                           GaudiSequencer,
                           L0TriggerTisTos,
                           LoKi__Hybrid__TupleTool,
                           OfflineVertexFitter,
                           PrintDecayTree,
                           PrintDecayTreeTool,
                           TaggingUtilsChecker,
                           TESCheck,
                           TriggerTisTos,
                           TupleToolAngles,
                           TupleToolDecay,
                           TupleToolDecayTreeFitterExtended,
                           TupleToolRecoStats,
                           TupleToolTagging,
                           TupleToolTISTOS,
                           TupleToolStripping,
                           TupleToolTrackPosition,
                           TupleToolTrigger,
                           TupleToolEventInfo,
                           TupleToolVtxIsoln,
                           TupleToolGeometry,
                           TupleToolTrackInfo,
                           TupleToolPid,
                           TupleToolMassHypo)

import re

from PhysSelPython.Wrappers import Selection, SelectionSequence
from StandardParticles import StdLoosePions, StdAllNoPIDsPions

from Configurables import TrackScaleState
from Configurables import TrackSmearState as SMEAR

from Configurables import CondDB

##################################### PRESELECTION #####################################

from Configurables import LoKi__HDRFilter as StripFilter

stripFilter = StripFilter( 'StripPassFilter',
                           Code = "HLT_PASS('StrippingB02DDBeauty2CharmLineDecision')",
                           Location= "/Event/Strip/Phys/DecReports" )
DaVinci().EventPreFilters = [stripFilter]



from PhysSelPython.Wrappers import DataOnDemand
inputDataOnDemandB2DD = DataOnDemand(Location = prefix + 'Phys/B02DDBeauty2CharmLine/Particles')

_tighterDs = FilterDesktop("TighterDs")

D1MassWindow = "( CHILDCUT( ADMASS('D+')<50*MeV, 1)) "
D2MassWindow = "( CHILDCUT( ADMASS('D+')<50*MeV, 2)) "
DMassWindowL = "(" + D1MassWindow + " & " + D2MassWindow + ")"


D1MassWindowA = "( CHILDCUT( ADMASS('D_s+')<50*MeV, 1)) "
D2MassWindowA = "( CHILDCUT( ADMASS('D+')<50*MeV, 2)) "
DMassWindowA = "(" + D1MassWindowA + " & " + D2MassWindowA + ")"


D1MassWindowB = "( CHILDCUT( ADMASS('D_s+')<50*MeV, 2)) "
D2MassWindowB = "( CHILDCUT( ADMASS('D+')<50*MeV, 1)) "
DMassWindowB = "(" + D1MassWindowB + " & " + D2MassWindowB + ")"

DMassWindow = "(" + DMassWindowL + "|" + DMassWindowB + "|" + DMassWindowA + ")"

_tighterDs.Code = DMassWindow
SelTighterDs = Selection("SelTighterDs", Algorithm = _tighterDs, RequiredSelections = [inputDataOnDemandB2DD])
SelSeqTighterDs = SelectionSequence("SelSeqTighterDs", TopSelection = SelTighterDs)
seqDs = SelSeqTighterDs.sequence()

##############################################################################################
##### Upgrade of _TRACK_GhostProb for downstream tracks
##############################################################################################
from STTools import STOfflineConf
STOfflineConf.DefaultConf().configureTools()
from Configurables import RefitParticleTracks
refitter = RefitParticleTracks()
refitter.DoFit = True
# shouldn't make a difference. either confirm that, or leave it as it is
refitter.DoProbability = True
refitter.UpdateProbability = True
refitter.ReplaceTracks = True
# shouldn't make a difference. either confirm that, or leave it as it is

## I never fully convinced myself whether the following three lines are
## necessary or not. Either way they're not wrong:
from Configurables import TrackInitFit, TrackMasterFitter
from TrackFitter.ConfiguredFitters import ConfiguredMasterFitter

refitter.addTool(TrackInitFit,"TrackInitFit")
refitter.TrackInitFit.addTool(TrackMasterFitter,"Fit")
ConfiguredMasterFitter( refitter.TrackInitFit.Fit )

tuple = DecayTreeTuple("B02DD")

#tuple.OutputLevel = 1
tuple.Inputs = [SelSeqTighterDs.outputLocation()]
#tuple.Inputs = ['/Event/Bhadron/Phys/B02DDBeauty2CharmLine/Particles']

tuple.Decay = "[B0 -> ^(D+ -> ^K- ^(pi+ || K+) ^(pi+ || K+)) ^(D- -> ^K+ ^(pi- || K-) ^(pi- || K-))]CC"
tuple.ReFitPVs = False


##############################################################################################
##### STANDARD TOOLS AND STRIPPING LINE
##############################################################################################
tuple_tools = ["TupleToolKinematic",
               "TupleToolPropertime",
               # "TupleToolEWTrackIsolation",  # advertised at analysis week, but not available?
               "TupleToolTrackIsolation",
               #"TupleToolConeIsolation",  # available from DV v36r5 onwards
               "TupleToolPrimaries"]

tuple_tools_mc = ["TupleToolMCTruth",
                  "TupleToolMCBackgroundInfo"]#,
                  #"TupleToolMCDecayTree"]

if DaVinci().Simulation == True:
  tt_mct = tuple.addTupleTool("TupleToolMCTruth")
  tt_mct.ToolList += ['MCTupleToolKinematic', 'MCTupleToolHierarchy']
  tuple_tools.extend(tuple_tools_mc)

tuple.ToolList = tuple_tools

tt_pid = tuple.addTupleTool("TupleToolPid")
tt_pid.Verbose = True

tt_trackpos = tuple.addTupleTool("TupleToolTrackPosition")
tt_trackpos.Z = 7500.

tt_recostats = tuple.addTupleTool("TupleToolRecoStats")
tt_recostats.Verbose = True

tt_geometry = tuple.addTupleTool("TupleToolGeometry")
tt_geometry.Verbose = False
tt_geometry.RefitPVs = False
tt_geometry.FillMultiPV = False

tt_trackinfo = tuple.addTupleTool("TupleToolTrackInfo") # matchChi2 etc.
tt_trackinfo.Verbose = True

tt_eventinfo = tuple.addTupleTool("TupleToolEventInfo")
tt_eventinfo.Verbose = True

tt_stripping = tuple.addTupleTool("TupleToolStripping")
tt_stripping.Verbose = True
tt_stripping.StrippingList = ['StrippingB02DDBeauty2CharmLineDecision']

branches = {}
#descriptor_B = "([B0 -> (D+ -> K- pi+ pi+) (D- -> K+ pi- pi-)]CC)"
descriptor_B = "([B0 -> (D+ -> K- (pi+ || K+) (pi+ || K+)) (D- -> K+ (pi- || K-) (pi- || K-))]CC)"
branches["B0"] = "^(" + descriptor_B + ")"

add_branches = { "Dplus"  : ( "D+", { #"substitute"                  : [ {"K-" : "K-"}, {"K-" : "pi-"}, {"pi+" : "K+"} ],
                                      #"LOKI_DOCA"                   : "ACUTDOCA",
                                      "LOKI_VertexSeparation_CHI2"  : "BPVVDCHI2" }),
                 "Dminus" : ( "D-", { #"substitute"                  : [ {"K+" : "K+"}, {"K+" : "pi+"}, {"pi-" : "K-"} ],
                                      #"LOKI_DOCA"                   : "ACUTDOCA",
                                      "LOKI_VertexSeparation_CHI2"  : "BPVVDCHI2" })}

for name_branch, value in add_branches.iteritems():
  branches[name_branch] = descriptor_B.replace("(" + value[0], "^(" + value[0])
  tuple.addTupleTool(TupleToolDecay, name=name_branch)
  branch_infos = value[1]
  if "substitute" in branch_infos:
    substitutions = branch_infos.pop("substitute")
    i = 0
    for substitution in substitutions:
      tt_masshypo = tuple.__getattr__(name_branch).addTupleTool("TupleToolMassHypo/TTMassHypo_" + str(i))
      tt_masshypo.PIDReplacements = substitution
      i += 1
  if len(branch_infos) > 0:
    tt_loki_br = tuple.__getattr__(name_branch).addTupleTool("LoKi::Hybrid::TupleTool/tt_loki_"+name_branch)
    tt_loki_br.Variables = branch_infos
  else:
    print "For branch " + name_branch + " no other LOKI variables specified."

branches["Dplus_Kminus"] = "[B0 -> (D+ -> ^K- (pi+ || K+) (pi+ || K+)) (D- -> K+ (pi- || K-) (pi- || K-))]CC"
branches["Dplus_piplus_or_Kplus_One"] = "[B0 -> (D+ -> K- ^(pi+ || K+) (pi+ || K+)) (D- -> K+ (pi- || K-) (pi- || K-))]CC"
branches["Dplus_piplus_or_Kplus_Two"] = "[B0 -> (D+ -> K- (pi+ || K+) ^(pi+ || K+)) (D- -> K+ (pi- || K-) (pi- || K-))]CC"
branches["Dminus_Kplus"] = "[B0 -> (D+ -> K- (pi+ || K+) (pi+ || K+)) (D- -> ^K+ (pi- || K-) (pi- || K-))]CC"
branches["Dminus_piminus_or_Kminus_One"] = "[B0 -> (D+ -> K- (pi+ || K+) (pi+ || K+)) (D- -> K+ ^(pi- || K-) (pi- || K-))]CC"
branches["Dminus_piminus_or_Kminus_Two"] = "[B0 -> (D+ -> K- (pi+ || K+) (pi+ || K+)) (D- -> K+ (pi- || K-) ^(pi- || K-))]CC"

tuple.addBranches(branches)

tuple.addTupleTool(TupleToolDecay, name="B0")


##############################################################################################
##### TAGGING (not available before Stripping 21)
##############################################################################################
#
#
#tt_tagging = None
#if DaVinci().Simulation == True:
# tt_tagging = tuple.addTupleTool("TupleToolTaggingMC")
#else:
# tt_tagging = tuple.B0.addTupleTool("TupleToolTagging", name = "B0")
## tt_tagging.OutputLevel = 2
#tt_tagging.useFTonDST = True
#
#if DaVinci().Simulation == True and DaVinci().DataType == '2012':
# from FlavourTagging.Tunings import TuneTool
# myTuneTool = TuneTool(tt_tagging,"Reco14_MC12","BTaggingTool")  # sets the Reco14_MC12 tuning (default is Reco14_2012)
#
#if DaVinci().Simulation == True and (DaVinci().DataType == "2015" or DaVinci().DataType == "2016"):
#    tt_tagging.TaggingToolName = "BTaggingTool/BsTaggingTool"
#    tt_tagging.addTool(BTaggingTool,name="BsTaggingTool")
#    #tt_tagging.BsTaggingTool.ForceSignalID = '"Bs"'   # required if you want to force the combination of OS with SSK
#    tt_tagging.Verbose = True
#    from FlavourTagging.Tunings import TuneTool
#    myTuneTool = TuneTool(tt_tagging,"Reco14_MC12","BsTaggingTool")  # sets the Reco14_MC12 tuning (default is Reco14_2012)
#
#tt_tagging.Verbose = True
#
##############################################################################################
##### TRIGGER
##############################################################################################

l0_lines = ['L0PhysicsDecision',
            'L0MuonDecision',
            'L0DiMuonDecision',
            'L0MuonHighDecision',
            'L0HadronDecision',
            'L0ElectronDecision',
            'L0PhotonDecision']


hlt1_lines = ['Hlt1TrackMVADecision',
	      'Hlt1TwoTrackMVADecision',
              #'Hlt1TrackMVALooseDecision',
              #'Hlt1TwoTrackMVALooseDecision',
	      'Hlt1TrackAllL0TightDecision',
              'Hlt1L0AnyDecision',
              'Hlt1GlobalDecision',
	      'Hlt1IncPhiDecision']


hlt2_lines = ['Hlt2GlobalDecision',
#              'Hlt2Topo2BodySimpleDecision',
#              'Hlt2Topo3BodySimpleDecision',
#              'Hlt2Topo4BodySimpleDecision',
              'Hlt2Topo2BodyDecision',
              'Hlt2Topo3BodyDecision',
              'Hlt2Topo4BodyDecision',
              'Hlt2TopoMu2BodyDecision',
              'Hlt2TopoMu3BodyDecision',
              'Hlt2TopoMu4BodyDecision',
              'Hlt2TopoE2BodyDecision',
              'Hlt2TopoE3BodyDecision',
              'Hlt2TopoE4BodyDecision',
#              'Hlt2IncPhiDecision',
#              'Hlt2IncPhiSidebandsDecision'
	     ]


trigger_lines = l0_lines + hlt1_lines + hlt2_lines
tt_trigger = tuple.addTupleTool("TupleToolTrigger")
tt_trigger.Verbose = True
tt_trigger.TriggerList = trigger_lines

tt_tistos = tuple.__getattr__("B0").addTupleTool("TupleToolTISTOS")
tt_tistos.Verbose = True
tt_tistos.TriggerList = trigger_lines


##############################################################################################
##### LOKI VARIABLES
##############################################################################################

loki_variables = {"LOKI_ENERGY"     : "E",
                  "LOKI_ETA"        : "ETA",
                  "LOKI_PHI"        : "PHI"}

tt_loki_general = tuple.addTupleTool("LoKi::Hybrid::TupleTool/tt_loki_general")
tt_loki_general.Variables = loki_variables

loki_variables_B = {"LOKI_FDCHI2"                 : "BPVVDCHI2",
                    "LOKI_FDS"                    : "BPVDLS",
                    "LOKI_DIRA"                   : "BPVDIRA",
                    ###### DecayTreeFitVariables
                    "LOKI_DTF_CTAU"               : "DTF_CTAU( 0, True )",
                    "LOKI_DTF_CTAU_NOPV"          : "DTF_CTAU( 0, False )",
                    "LOKI_DTF_CTAUS"              : "DTF_CTAUSIGNIFICANCE( 0, True )",
                    "LOKI_DTF_CHI2NDOF"           : "DTF_CHI2NDOF( True )",
                    "LOKI_DTF_CTAUERR"            : "DTF_CTAUERR( 0, True )",
                    "LOKI_DTF_CTAUERR_NOPV"       : "DTF_CTAUERR( 0, False )",
                    "LOKI_MASS_DplusConstr"       : "DTF_FUN ( M , True , 'D+' )" ,
                    "LOKI_MASS_Dplus_NoPVConstr"  : "DTF_FUN ( M , False , 'D+' )" ,
                    "LOKI_MASS_DminusConstr"      : "DTF_FUN ( M , True , 'D-' )" ,
                    "LOKI_MASS_Dminus_NoPVConstr" : "DTF_FUN ( M , False , 'D-' )" ,
                    "LOKI_MASSERR_DplusConstr"    : "sqrt(DTF_FUN ( M2ERR2 , True , 'D+' ))" ,
                    "LOKI_MASSERR_DminusConstr"   : "sqrt(DTF_FUN ( M2ERR2 , True , 'D-' ))" ,
                    "LOKI_DTF_VCHI2NDOF"          : "DTF_FUN ( VFASPF(VCHI2/VDOF) , True )",
                    "LOKI_DTF_CTAU_D1"            : "DTF_CTAU(1, True)",
                    "LOKI_DTF_CTAU_D2"            : "DTF_CTAU(2, True)",
                    "LOKI_DTF_CTAUERR_D1"         : "DTF_CTAUERR(1, True)",
                    "LOKI_DTF_CTAUERR_D2"         : "DTF_CTAUERR(2, True)",
                    "LOKI_MASS_DDConstr"          : "DTF_FUN ( M , True , strings ( ['D+','D-'] ) )" ,
                    "LOKI_MASSERR_DDConstr"       : "sqrt(DTF_FUN ( M2ERR2 , True , strings(['D+','D-'])))"}

tt_loki_B = tuple.__getattr__("B0").addTupleTool("LoKi::Hybrid::TupleTool/tt_loki_B")
tt_loki_B.Variables = loki_variables_B

##############################################################################################
##### CONSTRAINTS
##############################################################################################

#No constraints at all
Fit = tuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/Fit")
Fit.Verbose = True
#Fit.OutputLevel = VERBOSE
Fit.UpdateDaughters = True
Fit.constrainToOriginVertex = False

#Constraint only on PV 
FitPVConst = tuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/FitPVConst")
FitPVConst.Verbose = True
#FitPVConst.OutputLevel = VERBOSE
FitPVConst.UpdateDaughters = True
FitPVConst.constrainToOriginVertex = True

#Constraint on daughters but not on PV
FitDaughtersConst = tuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/FitDDConst")
FitDaughtersConst.Verbose = True
#FitDaughtersConst.OutputLevel = VERBOSE
FitDaughtersConst.UpdateDaughters = True
FitDaughtersConst.daughtersToConstrain = ["D+","D-"]
FitDaughtersConst.constrainToOriginVertex = False

#Constraint on daughters and constraint to OriginPV
FitDaughtersPVConst = tuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/FitDDPVConst")
FitDaughtersPVConst.Verbose = True
#FitDaughtersPVConst.OutputLevel = VERBOSE
FitDaughtersPVConst.UpdateDaughters = True
FitDaughtersPVConst.daughtersToConstrain = ["D+","D-"]
FitDaughtersPVConst.constrainToOriginVertex = True


###########################################################
seqB2DD                  = GaudiSequencer('seqB2DD')
seqB2DD.Members         += [tuple]
seqB2DD.ModeOR           = True
seqB2DD.ShortCircuit     = False

evtTuple                 = EventTuple()
evtTuple.ToolList       += ["TupleToolEventInfo", "TupleToolTrigger"]

DaVinci().EvtMax     = max_events
DaVinci().SkipEvents = skip_events
DaVinci().Lumi       = True
if DaVinci().Simulation == False:
  DaVinci().InputType  = 'MDST'
  #DaVinci().InputType  = 'DST' #stripping28
else:
  DaVinci().InputType  = 'DST'

if DaVinci().Simulation == False and DaVinci().DataType == "2016":
  CondDB( LatestGlobalTagByDataType = "2016" )
if DaVinci().Simulation == False and DaVinci().DataType == "2015":
  CondDB( LatestGlobalTagByDataType = "2015" )

DaVinci().TupleFile       = "DTT.root" # Ntuple

subseq_annpid = GaudiSequencer('SeqANNPID')

from Configurables import ChargedProtoANNPIDConf
conf_ann_pid = ChargedProtoANNPIDConf('ANNPIDConf')
conf_ann_pid.RecoSequencer = subseq_annpid

if DaVinci().Simulation == False:
  scaler = TrackScaleState('scaler')
  if update_track_ghostprob:
    DaVinci().UserAlgorithms += [subseq_annpid, refitter]
  DaVinci().UserAlgorithms += [scaler]
else:
  smearer = SMEAR("StateSmear")
  DaVinci().UserAlgorithms += [smearer]


DaVinci().UserAlgorithms += [evtTuple, seqDs, seqB2DD]



#Read .mdst file
#EventSelector().Input = ['/fhgfs/users/delten/test/00052191_00002507_1.bhadron.mdst']
#EventSelector().Input = ['/fhgfs/users/delten/test/00049671_00000193_1.bhadron.mdst']


############################# MINIMAL EXAMPLE ###############################################


#from Configurables import (DecayTreeTuple, DaVinci)
#from Configurables import TrackSmearState, BTaggingTool
#from DecayTreeTuple.Configuration import *
#from Configurables import CondDB
## Stream and stripping line we want to use
##stream = 'bhadron'
#line = 'StrippingB02DDBeauty2CharmLine'
#dtt = DecayTreeTuple('B02DD')
#dtt.Inputs = ['/Event/Bhadron/Phys/B02DDBeauty2CharmLine/Particles']
#dtt.Decay = "[B0 -> ^(D+ -> ^K- ^pi+ ^pi+) ^(D- -> ^K+ ^pi- ^pi-)]CC"
#dtt.ToolList =  [ "TupleToolGeometry"
#                 ,"TupleToolTrigger"
#                 , "TupleToolTrackInfo"
#                 , "TupleToolAngles"
#                 , "TupleToolPid"
#                 , "TupleToolMCTruth"
#                 , "MCTupleToolKinematic"
#                 , "TupleToolKinematic"
#]
# Configure DaVinci
#DaVinci().UserAlgorithms += [dtt]
#DaVinci().InputType = 'mDST'
#DaVinci().TupleFile = 'B02DD.root'
#DaVinci().PrintFreq = 1000
#DaVinci().DataType = '2015'
#DaVinci().Simulation = True
#DaVinci().EvtMax = -1
#DaVinci().Lumi = not DaVinci().Simulation
#CondDB( LatestGlobalTagByDataType = "2015" )
##Read .dst file
#EventSelector().Input = ['/fhgfs/users/delten/test/00049671_00000193_1.bhadron.mdst']
