import os
#runs on SetupGanga
sim = False 
#sim = True      
if sim==True:
  #jobname            = 'B_DsD_2011_MC'
  #jobname            = 'B_DsD_KKpi_2011_MC'
  #jobname            = 'B02DD_2016_MC'
  jobname           = 'Bs2DD_MC'
  #davinci_ver        = 'v41r2'
  davinci_ver        = 'v41r4p1' #B02DD Run2 MC
  tuplemakerbase     = '/home/delten/repos/tuplemaker/'
  #basepath           = tuplemakerbase + 'inputs_and_tags/simulated_data/MC_11296012_Bd_DsD==DDALITZ,DecProdCut_pcut1600MeV_'
  #basepath           = tuplemakerbase + 'inputs_and_tags/simulated_data/MC_11296021_Bd_DsD,KKpi==DDALITZ,DecProdCut_pcut1600MeV_'
  #basepath           = tuplemakerbase + 'inputs_and_tags/simulated_data/MC_11396010_Bd_DD,Kpipi,KKpi=CPV,DDALITZ,DecProdCut_' #B02DD MC
  basepath            = tuplemakerbase + 'inputs_and_tags/simulated_data/MC_13396010_Bs_DD,Kpipi,KKpi=DDALITZ,DecProdCut_' #Bs2DD
  main_opts_dir      = tuplemakerbase + 'main_options/'
  #selectionfile_path = main_opts_dir + 'B2DD_SelectionDescriptorChange.py' #for Run I
  #selectionfile_path = main_opts_dir + 'B02DD_Selection.py' #for Run II
  selectionfile_path = main_opts_dir + 'Bs2DD_Selection.py' #for Run II, Bs2DD MC
  simver_and_years = [ ['Sim09b', '2015']]
  polarities       = ['MD','MU']
  generators       = ['P8'] 
  inputfile_suffix = '_files.py'
  tags_suffix      = '_tags.py'
  jname_and_optionfiles_list = []
  data_or_sim_opts = main_opts_dir + 'DV_Simulation.py'
  for simver_and_year in simver_and_years:
    for generator in generators:
      for polarity in polarities:
        specific_jobname = jobname + '_' + simver_and_year[1] + '_' + polarity + generator
        specific_base    = basepath + simver_and_year[0] + '_' + simver_and_year[1] + '/' + polarity + generator
        datatype_opts    = main_opts_dir + 'DV_' + simver_and_year[1] + '.py'
        inputfiles_path  = specific_base + inputfile_suffix
        tags_path = specific_base + tags_suffix
        if not os.path.exists(data_or_sim_opts):
          print(data_or_sim_opts + ' not found - therefore skipping this job')
          continue
        if not os.path.exists(datatype_opts):
          print(datatype_opts + ' not found - therefore skipping this job')
          continue
        if not os.path.exists(inputfiles_path):
          print(inputfiles_path + ' not found - therefore skipping this job')
          continue
        if not os.path.exists(tags_path):
          print(tags_path + ' not found - therefore skipping this job')
          continue
        if not os.path.exists(selectionfile_path):
          print(selectionfile_path + ' not found - therefore skipping this job')
          continue
        else:
          jname_and_optionfiles_list.append([specific_jobname,
                                             inputfiles_path,
                                             tags_path,
                                             data_or_sim_opts,
                                             datatype_opts,
                                             selectionfile_path]) 
  for i in jname_and_optionfiles_list:
    print i
  for jname_and_optionfiles in jname_and_optionfiles_list:
    jobname       = jname_and_optionfiles[0]
    inputfile     = jname_and_optionfiles[1]
    tagsfile      = jname_and_optionfiles[2]
    dataorsimfile = jname_and_optionfiles[3]
    datatypefile  = jname_and_optionfiles[4]
    selectionfile = jname_and_optionfiles[5]
  
    print('Jobname:   ' + jobname  )
    print('inputfile: ' + inputfile)
    print('tagsfile:  ' + tagsfile )
    print('datatype:  ' + datatypefile)
    print('dataorsim: ' + dataorsimfile )
  
    j = Job()
    j.name    = jobname 
    j.comment = jobname 
    j.application = DaVinci()
    j.application.optsfile = [tagsfile, datatypefile, dataorsimfile,  selectionfile]
    j.application.version = davinci_ver
    #j.application.platform='x86_64-slc6-gcc48-opt'
    j.inputdata = j.application.readInputData(inputfile)
    
    j.splitter = SplitByFiles(filesPerJob=2, maxFiles = -1)
    j.outputfiles = ["DTT.root"]
    #j.do_auto_resubmit = True
    
    j.backend = Dirac()
    
    #j.submit()
    

############################################## data ##################################

if sim==False:
  jobname            = 'B02DD_data_stripping24'
  #davinci_ver        = 'v41r2'
  davinci_ver        = 'v41r4p1'
  tuplemakerbase     = '/home/delten/repos/tuplemaker/'
  basepath           = tuplemakerbase + 'inputs_and_tags/real_data/stripping24/bhadron/'
  #basepath           = tuplemakerbase + 'inputs_and_tags/real_data/stripping26/bhadron/'
  #basepath           = tuplemakerbase + 'inputs_and_tags/real_data/stripping28/bhadron/'
  main_opts_dir      = tuplemakerbase + 'main_options/'
  selectionfile_path = main_opts_dir + 'B02DD_Selection.py'
  
  years = ['2015']
  polarities       = ['MD','MU']
  inputfile_suffix = '_files.py'
  tags_suffix      = '_tags.py'
  
  jname_and_optionfiles_list = []
  data_or_sim_opts = main_opts_dir + 'DV_Data.py'
  for year in years:
    for polarity in polarities:
      specific_jobname = jobname + '_' + year + '_' + polarity
      specific_base    = basepath + year + '/' + polarity
      datatype_opts    = main_opts_dir + 'DV_' + year + '.py'
      inputfiles_path  = specific_base + inputfile_suffix
      tags_path = specific_base + tags_suffix
      if not os.path.exists(data_or_sim_opts):
        print(data_or_sim_opts + ' not found - therefore skipping this job')
        continue
      if not os.path.exists(datatype_opts):
        print(datatype_opts + ' not found - therefore skipping this job')
        continue
      if not os.path.exists(inputfiles_path):
        print(inputfiles_path + ' not found - therefore skipping this job')
        continue
      if not os.path.exists(tags_path):
        print(tags_path + ' not found - therefore skipping this job')
        continue
      if not os.path.exists(selectionfile_path):
        print(selectionfile_path + ' not found - therefore skipping this job')
        continue
      else:
        jname_and_optionfiles_list.append([specific_jobname,
                                           inputfiles_path,
                                           tags_path,
                                           data_or_sim_opts,
                                           datatype_opts,
                                           selectionfile_path]) 
  
  for i in jname_and_optionfiles_list:
    print i
  
  for jname_and_optionfiles in jname_and_optionfiles_list:
    jobname       = jname_and_optionfiles[0]
    inputfile     = jname_and_optionfiles[1]
    tagsfile      = jname_and_optionfiles[2]
    dataorsimfile = jname_and_optionfiles[3]
    datatypefile  = jname_and_optionfiles[4]
    selectionfile = jname_and_optionfiles[5]
  
    print('Jobname:   ' + jobname  )
    print('inputfile: ' + inputfile)
    print('tagsfile:  ' + tagsfile )
    print('datatype:  ' + datatypefile)
    print('dataorsim: ' + dataorsimfile )
    
    j = Job()
    j.name    = jobname 
    j.comment = jobname 
    j.application = DaVinci()
    j.application.optsfile = [tagsfile, datatypefile , dataorsimfile, selectionfile]
    j.application.version = davinci_ver
    j.inputdata = j.application.readInputData(inputfile)
    
    j.splitter = SplitByFiles(filesPerJob=10, maxFiles = -1)
    j.outputfiles = [LocalFile("DTT.root")]
    #j.do_auto_resubmit = True
    
    ## im Grid  
    j.backend = Dirac()
    
    ## auf Phido
    # j.backend = PBS()
  
    ## auf interaktiven Maschinen
    #j.backend = Local()
  
    #j.backend.extraopts = '-l walltime=4:00:00,vmem=4gb'
    #j.backend.queue = "FhGfs"
    
    #j.submit()  
  
