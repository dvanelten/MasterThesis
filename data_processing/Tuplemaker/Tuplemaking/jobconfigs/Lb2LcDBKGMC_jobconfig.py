# run gangajob with  lb-run Ganga v602r3 ganga B2DstD_MCjobconfig.py
import os

basepath = '/home/delten/repos/tuplemaker/'

eventtypes =	[['Lb2LcD', 'inputs_and_tags/simulated_data/MC_15196011_Lb2LcD,LcKppi=DDALITZ,DecProdCut_Sim09c_']]
years = ['2016']
polarities = ['MagUp', 'MagDown']


# Application
myApp = GaudiExec()
myApp.directory = "/home/delten/cmtuser/DaVinciDev_v41r4p1"


# Choose PBS backend and specify walltime
bck = Dirac()


# Split into subjobs, defining maximum number of input files to analyse
# and number of input files per subjob
splitter = SplitByFiles()
splitter.maxFiles = -1
splitter.filesPerJob = 1

for eventtype in eventtypes:
	tagpath = basepath + eventtype[1]

	for year in years:
		for polarity in polarities:
			jobname = 'MC_' + eventtype[0] + '_' + polarity + '_' + year
			jobselectionfile = basepath + 'main_options/B02DD_Selection.py'
                        #jobselectionfile = basepath + 'main_options/Bs2DD_Selection.py'
			jobyear = basepath + 'main_options/DV_' + year + '.py'
			jobdataorsim = basepath + 'main_options/DV_Simulation.py'
			jobpolarity = tagpath + year + '/' + polarity + 'P8_tags.py'
			jobinputfile = tagpath + year + '/'+ polarity + 'P8_files.txt'

			if not os.path.exists(jobinputfile):
				print(jobinputfile + ' not found - therefore skipping this job\n')
				continue

			jobinput = open(jobinputfile,'r').read().replace('\n','')

			if not os.path.exists(jobselectionfile):
				print(jobselectionfile + ' not found - therefore skipping this job\n')
				continue
			if not os.path.exists(jobyear):
				print(jobyear + ' not found - therefore skipping this job\n')
				continue
			if not os.path.exists(jobdataorsim):
				print(jobdataorsim + ' not found - therefore skipping this job\n')
				continue
			if not os.path.exists(jobpolarity):
				print(jobpolarity + ' not found - therefore skipping this job\n')
				continue

			print('jobname:\t\t'		+ jobname)
			print('inputfile:\t\t'		+ jobinputfile)
			print('input:\t\t\t'		+ jobinput)
			print('tags:\t\t\t'			+ jobpolarity)
			print('selectionfile:\t\t'	+ jobselectionfile)
			print('year:\t\t\t'			+ jobyear)
			print('dataorsim:\t\t'		+ jobdataorsim + '\n')

			myApp.options = [jobyear, jobdataorsim, jobpolarity, jobselectionfile]
			j = Job( name = jobname, comment = jobname, backend = bck, splitter = splitter)
			j.do_auto_resubmit = False
			j.application = myApp
			j.outputfiles = [LocalFile("DTT.root")]
			j.inputdata = BKQuery ( jobinput, dqflag = 'OK' ).getDataset()

