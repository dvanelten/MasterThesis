from Configurables import (DecayTreeTuple, DaVinci)
from Configurables import TrackSmearState, BTaggingTool
from DecayTreeTuple.Configuration import *
from Configurables import CondDB
# Stream and stripping line we want to use
#stream = 'Bhadron'
line = 'StrippingB02DDBeauty2CharmLine'
dtt = DecayTreeTuple('B02DD_minimal')
dtt.Inputs = ['/Event/Bhadron/Phys/B02DDBeauty2CharmLine/Particles']
#dtt.Inputs = ['BhadronCompleteEvent/Phys/B02DDBeauty2CharmLine/Particles'] #stripping28
dtt.Decay = "[B0 -> ^(D+ -> ^K- ^(pi+ || K+) ^(pi+ || K+)) ^(D- -> ^K+ ^(pi- || K-) ^(pi- || K-))]CC"
dtt.ToolList =  [ "TupleToolGeometry"
                 ,"TupleToolTrigger"
                 , "TupleToolTrackInfo"
                 , "TupleToolAngles"
                 , "TupleToolPid"
                 , "TupleToolMCTruth"
                 , "MCTupleToolKinematic"
                 , "TupleToolKinematic"
]
# Configure DaVinci
DaVinci().UserAlgorithms += [dtt]
DaVinci().InputType = 'mDST'
#DaVinci().InputType = 'DST' #stripping28
DaVinci().TupleFile = 'B02DD.root'
DaVinci().PrintFreq = 1000
DaVinci().DataType = '2015'
#DaVinci().Simulation = True
DaVinci().EvtMax = 15000
#DaVinci().Lumi = not DaVinci().Simulation
#CondDB( LatestGlobalTagByDataType = "2015" )
#Read .dst file
EventSelector().Input = ['/fhgfs/users/delten/test/00049575_00000386_1.bhadron.mdst']
#EventSelector().Input = ['/fhgfs/users/delten/test/00059560_00000011_1.bhadroncompleteevent.dst']
