// from STL
#include <tuple>
#include <list>

// from ROOT
#include "TRandom3.h"
#include "TCut.h"

// from DooCore
#include "doocore/io/MsgStream.h"
#include "doocore/config/Summary.h"

// from DooSelection
#include "dooselection/reducer/Reducer.h"
#include "dooselection/reducer/Reducer.h"
#include "dooselection/reducer/ReducerLeaf.h"
#include "dooselection/reducer/KinematicReducerLeaf.h"

#include "fstream"
#include "vector"
#include "fstream"
#include <iostream>
#include <stdio.h>

using namespace dooselection::reducer;
using namespace doocore::io;
using namespace std;

// forward declarations
// typedef tuple: head, daughters, stable particles, isMC, isFlat
int main(int argc, char * argv[]){
	sinfo << "-info-  \t" << "VariablesRemoverReducer \t" << "Welcome!" << endmsg; 
	std::string inputfile, inputtree, outputfile, outputtree, decay_channel, list;
	if (argc == 6){
		inputfile = argv[1];
		inputtree = argv[2];
		outputfile = argv[3];
		outputtree = argv[4];
		list = argv[5];
	}
	else{
		serr << "-ERROR- \t" << "VariablesRemoverReducer \t" << "Parameters needed:" << endmsg;
		serr << "-ERROR- \t" << "VariablesRemoverReducer \t"<< "input_file_name input_tree_name output_file_name output_tree_name variablestoremove_file_name" << endmsg;
		return 1;
	}


	vector<string> list_vec;
	string line;
	ifstream list_stream (list);
	if (list_stream.is_open())
	{
		while (getline(list_stream, line))
		{
			list_vec.push_back (line);
		}
		list_stream.close();
	}
	else
	{
		cout << "Unable to open " << list << endl;
		return 1;
	}

	dooselection::reducer::Reducer reducer;

	reducer.set_input_file_path(inputfile);
	reducer.set_input_tree_path(inputtree);
	reducer.set_output_file_path(outputfile);
	reducer.set_output_tree_path(outputtree);

	for (int i = 0; i < list_vec.size(); ++i)
	{
		reducer.add_branch_omit(list_vec[i].c_str());
	}

	reducer.Initialize();
	reducer.Run();
	reducer.Finalize();
}

